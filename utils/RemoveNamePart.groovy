

class RemoveNamePart {

    def str = """"let ParkingCategory = Category(name: LocalizedString("parking"), icon: "ic_parking")
                let ShoppingCategory = Category(name: LocalizedString("shopping"), icon: "ic_shopping")
                let SportsCategory = Category(name: LocalizedString("sports"), icon: "ic_sports")
                let BarCategory = Category(name: LocalizedString("bar"), icon: "ic_bar")
                let AnimalsCategory = Category(name: LocalizedString("animals"), icon: "ic_animals")
                let CafeCategory = Category(name: LocalizedString("cafe"), icon: "ic_coffee")
                let CinemaCategory = Category(name: LocalizedString("cinema"), icon: "ic_cinema")
                let PubCategory = Category(name: LocalizedString("pub"), icon: "ic_pub")
                let BeachCategory = Category(name: LocalizedString("beach"), icon: "ic_beach")
                let DoctorCategory = Category(name: LocalizedString("doctor"), icon: "ic_doctor")
                let CarRepairCategory = Category(name: LocalizedString("car_rapair"), icon: "ic_car_repair")
                let GasStationCategory = Category(name: LocalizedString("gas_station"), icon: "ic_gas_station")
                let FastFoodCategory = Category(name: LocalizedString("fast_food"), icon: "ic_fast_food")
                let FitnessCategory = Category(name: LocalizedString("fitness"), icon: "ic_fitness")
                let GroceryCategory = Category(name: LocalizedString("grocery"), icon: "ic_grocery")
                let HotelCategory = Category(name: LocalizedString("hotel"), icon: "ic_hotel")
                let LibraryCategory = Category(name: LocalizedString("library"), icon: "ic_library")
                let MusicCategory = Category(name: LocalizedString("music_club"), icon: "ic_music")
                let ParkCategory = Category(name: LocalizedString("park"), icon: "ic_park")
                let PetsCategory = Category(name: LocalizedString("pets"), icon: "ic_pets")
                let DefauktCategory = Category(name: LocalizedString("default"), icon: "default_marker")
                let UnknownCategory = Category(name: LocalizedString("unknown"), icon: "default_marker")"""

    void printCategoryEnumList () {
        str.eachLine{line, number-> 
                        def categoryName = line.split()[1]
                        print(sprintf('case %1$s.name: return %1$s\n', [categoryName]))
                    }
    } 

     void printCategoryArray () {
        str.eachLine{line, number-> 
                        def categoryName = line.split()[1]
                        print(sprintf('%1$s,\n', [categoryName]))
                    }
    } 

    static void main(String[] args)  {
        def instance = new RemoveNamePart()
        instance.printCategoryArray()
    }
}