//
//  MapCenterRepo.swift
//  MyStreetPlaces
//
//  Created by Oleksii.Kuropiatnyk on 28/04/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import GoogleMaps
import RxSwift

protocol MapCenterRepo {
    func observe() -> Observable<Optional<CLLocation>>

    func update(center: CLLocation) -> Completable
}

class MapCenterRepoImpl: MapCenterRepo {
    let centerSubject = BehaviorSubject<Optional<Loc>>(value: Optional.none)

    func observe() -> Observable<Optional<CLLocation>> {
        return centerSubject.map {loc in
            if let res = loc {
                return CLLocation(latitude: res.lat, longitude: res.lng)
            } else {return nil}
        }
    }

    func update(center: CLLocation) -> Completable {
        return Completable.create { sub in
            let lat = center.coordinate.latitude
            let lng = center.coordinate.longitude
            self.centerSubject.onNext(Optional.some(Loc(lat: lat, lng: lng)))
            sub(.completed)
            return Disposables.create()
        }
    }
}

struct Loc {
    let lat: Double
    let lng: Double
}
