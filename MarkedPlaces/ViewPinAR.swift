//
//  ViewPinAR.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 09/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation

protocol ViewPinAction {}
protocol ViewPinResult {}

struct ViewPin {

    struct Load : ViewPinAction { let pinId:String }
    struct Back : ViewPinAction {}
    struct Edit : ViewPinAction {}
    struct Navigate : ViewPinAction {}
    struct Share : ViewPinAction { let screenName: String }

    struct LoadedResult : ViewPinResult { let pin: Pin }

    struct Model { var pin: Pin? }
}
