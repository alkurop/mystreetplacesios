//
// Created by Alexey Kuropiantnyk on 29/05/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import iOSMvi

class OptionsToolbarPresenter: Presenter {

    typealias ViewState = OptionsToolbar.ViewModel
    typealias Result = OptionsToolbarResult
    typealias Action = OptionsToolbarAction
    typealias Interactor1 = OptionsToolbarInteractor

    var interactor: OptionsToolbarInteractor

    init(interactor: OptionsToolbarInteractor) {
        self.interactor = interactor
    }

    func resultToViewStateMapper() -> (ViewState, Result) -> (ViewState) {
        return { oldState, result in
            let viewModel: ViewState
            switch result {
                case is OptionsToolbar.HasUserResult:
                    viewModel = ViewState(shouldShowLoginButton: false)
                case is OptionsToolbar.NoUserResult:
                    viewModel = ViewState(shouldShowLoginButton: true)
                default: viewModel = oldState
            }
            return viewModel
        }
    }

    func defaultViewState() -> ViewState {
        return ViewState(shouldShowLoginButton: true)
    }
}
