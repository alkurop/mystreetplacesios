//
//  Category.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 16/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import UIKit

struct Category: Equatable {
    let name: String
    let icon: String

    static func ==(lhs: Category, rhs: Category) -> Bool {
        return lhs.name == rhs.name
    }
}

let ParkingCategory = Category(name: LocalizedString("parking"), icon: "ic_parking")
let ShoppingCategory = Category(name: LocalizedString("shopping"), icon: "ic_shopping")
let SportsCategory = Category(name: LocalizedString("sports"), icon: "ic_sports")
let BarCategory = Category(name: LocalizedString("bar"), icon: "ic_bar")
let AnimalsCategory = Category(name: LocalizedString("animals"), icon: "ic_animals")
let CafeCategory = Category(name: LocalizedString("cafe"), icon: "ic_coffee")
let CinemaCategory = Category(name: LocalizedString("cinema"), icon: "ic_cinema")
let PubCategory = Category(name: LocalizedString("pub"), icon: "ic_pub")
let BeachCategory = Category(name: LocalizedString("beach"), icon: "ic_beach")
let DoctorCategory = Category(name: LocalizedString("doctor"), icon: "ic_doctor")
let CarRepairCategory = Category(name: LocalizedString("car_rapair"), icon: "ic_car_repair")
let GasStationCategory = Category(name: LocalizedString("gas_station"), icon: "ic_gas_station")
let FastFoodCategory = Category(name: LocalizedString("fast_food"), icon: "ic_fast_food")
let FitnessCategory = Category(name: LocalizedString("fitness"), icon: "ic_fitness")
let GroceryCategory = Category(name: LocalizedString("grocery"), icon: "ic_grocery")
let HotelCategory = Category(name: LocalizedString("hotel"), icon: "ic_hotel")
let LibraryCategory = Category(name: LocalizedString("library"), icon: "ic_library")
let MusicCategory = Category(name: LocalizedString("music_club"), icon: "ic_music")
let ParkCategory = Category(name: LocalizedString("park"), icon: "ic_park")
let PetsCategory = Category(name: LocalizedString("pets"), icon: "ic_pets")
let DefaultCategory = Category(name: LocalizedString("default"), icon: "default_marker")
let UnknownCategory = Category(name: LocalizedString("unknown"), icon: "default_marker")

let categoryCollection = [
    DefaultCategory,
    AnimalsCategory,
    BarCategory,
    BeachCategory,
    CafeCategory,
    CarRepairCategory,
    CinemaCategory,
    DoctorCategory,
    FastFoodCategory,
    FitnessCategory,
    GasStationCategory,
    GroceryCategory,
    HotelCategory,
    LibraryCategory,
    MusicCategory,
    ParkCategory,
    ParkingCategory,
    PetsCategory,
    PubCategory,
    ShoppingCategory,
    SportsCategory
]

func getCategoryByName(name: String?) -> Category {
    switch name {
        case ParkingCategory.name: return ParkingCategory
        case ShoppingCategory.name: return ShoppingCategory
        case SportsCategory.name: return SportsCategory
        case BarCategory.name: return BarCategory
        case AnimalsCategory.name: return AnimalsCategory
        case CafeCategory.name: return CafeCategory
        case CinemaCategory.name: return CinemaCategory
        case PubCategory.name: return PubCategory
        case BeachCategory.name: return BeachCategory
        case DoctorCategory.name: return DoctorCategory
        case CarRepairCategory.name: return CarRepairCategory
        case GasStationCategory.name: return GasStationCategory
        case FastFoodCategory.name: return FastFoodCategory
        case FitnessCategory.name: return FitnessCategory
        case GroceryCategory.name: return GroceryCategory
        case HotelCategory.name: return HotelCategory
        case LibraryCategory.name: return LibraryCategory
        case MusicCategory.name: return MusicCategory
        case ParkCategory.name: return ParkCategory
        case PetsCategory.name: return PetsCategory
        case DefaultCategory.name: return DefaultCategory
        case nil: return DefaultCategory
        case UnknownCategory.name: return UnknownCategory
        default:
            return UnknownCategory
    }
}
