//
//  HomeInteractor.swift
//  MyStreetPlaces
//
//  Created by Oleksii.Kuropiatnyk on 11/04/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import GoogleMaps
import iOSMvi

class HomeInteractor: Interactor {
    typealias Action = HomeAction
    typealias Result = HomeResult

    private let scheduler: ConcurrentDispatchQueueScheduler
    private let pinsRepo: PinsRepo
    private let homeRouter: HomeRouter
    private let mapCenterRepo: MapCenterRepo
    private let mapProjectionRepo: LatestProjectionRepo
    private let requestFocusRepo: RequestMapFocusRepo
    
    init(scheduler: ConcurrentDispatchQueueScheduler,
         pinsRepo: PinsRepo,
         homeRouter: HomeRouter,
         mapCenterRepo: MapCenterRepo,
         mapProjectionRepo: LatestProjectionRepo,
         requestFocusRepo: RequestMapFocusRepo) {

        self.scheduler = scheduler
        self.pinsRepo = pinsRepo
        self.homeRouter = homeRouter
        self.mapCenterRepo = mapCenterRepo
        self.mapProjectionRepo = mapProjectionRepo
        self.requestFocusRepo = requestFocusRepo
    }

    func provideProcessor() -> ComposeTransformer<HomeAction, HomeResult> {
        return ComposeTransformer { upstream in
            return upstream.observeOn(self.scheduler)
                    .compose(ComposeTransformer { upstream in
                        return Observable.merge([
                            upstream.filter { $0 is Home.Initial }.map { $0 as! Home.Initial }
                                    .compose(self.initial),
                            upstream.filter { $0 is Home.DropClick }.map { $0 as! Home.DropClick }
                                    .compose(self.dropClick),
                            upstream.filter { $0 is Home.LoadPins }.map { $0 as! Home.LoadPins }
                                    .compose(self.loadPins),
                            upstream.filter { $0 is Home.MapCenterUpdate }.map { $0 as! Home.MapCenterUpdate }
                                    .compose(self.updateMap),
                            upstream.filter { $0 is Home.ReloadPins }.map { $0 as! Home.ReloadPins }
                                    .compose(self.reloadPins),
                            upstream.filter { $0 is Home.OpenPinDetails }.map { $0 as! Home.OpenPinDetails }
                                    .compose(self.viewPin),
                            upstream.filter { $0 is Home.DropWithLocation }.map { $0 as! Home.DropWithLocation }
                                    .compose(self.dropWithLocation),
                            upstream.filter { $0 is Home.SearchClick }.map { $0 as! Home.SearchClick }
                                    .compose(self.openSearch)
                        ])
                    })
        }
    }
    
    lazy var initial = {
        ComposeTransformer {
            $0.flatMap { (it: Home.Initial) -> Observable<HomeResult> in
               self
                .requestFocusRepo
                .observe()
                .flatMap { (location : Optional<CLLocation>) -> Observable<HomeResult> in
                    if let nnLocation = location {
                        return Observable.just(Home.FosucTo(location: nnLocation))
                    }
                    else {
                        return Observable.empty()
                    }
                }
            }
        }
    }()

    lazy var dropClick = {
        ComposeTransformer<HomeAction, HomeResult> {
            $0.flatMap { action in self.mapCenterRepo
                .observe()
                .take(1) }
                .flatMap { (it: CLLocation?) -> Observable<HomeResult> in
                    if it != nil {
                        return self.homeRouter.dropPin(location: it!).andThen(Observable.empty())
                    } else {
                        return Observable.just(Home.ShowmapCenterUnknownResult())
                    }
            }
        }
    }()

    lazy var dropWithLocation = {
        ComposeTransformer<Home.DropWithLocation, HomeResult> {
            $0.flatMap { action in
                return self.homeRouter
                        .dropPin(location: action.location)
                        .andThen(Observable.empty())
            }
        }
    }()

    lazy var loadPins = {
        ComposeTransformer {
            $0.flatMap { (it: Home.LoadPins) -> Observable<HomeResult> in
                Observable.merge(
                        self.mapProjectionRepo.update(projection: it.bounds)
                                .andThen(Observable.empty()),

                        self.pinsRepo
                                .getPins(projection: it.bounds)
                                .asObservable()
                                .map { pins in Home.PinsLoadedResult(pins: pins) }
                )
            }
        }
    }()

    lazy var updateMap = {
        ComposeTransformer {
            $0.flatMap { (it: Home.MapCenterUpdate) -> Observable<HomeResult> in
                self.mapCenterRepo
                        .update(center: it.center)
                        .andThen(Observable.empty())
            }
        }
    }()

    lazy var reloadPins = {
        ComposeTransformer {
            $0.flatMap { (it: Home.ReloadPins) -> Observable<HomeResult> in
                self.mapProjectionRepo
                        .observe()
                        .take(1)
                        .flatMap { (value: Optional<[CLLocation]>) -> Observable<HomeResult> in
                            if let castValue = value {
                                return self.pinsRepo.getPins(projection: castValue)
                                        .asObservable()
                                        .map { pins in Home.PinsLoadedResult(pins: pins) }
                            } else {
                                return Observable.empty()
                            }
                        }
            }
        }
    }()

    lazy var viewPin = {
        ComposeTransformer {
            $0.flatMap { (it: Home.OpenPinDetails) -> Observable<HomeResult> in
                self.homeRouter.viewPin(id: it.pinId)
                        .andThen(Observable.empty())
            }
        }
    }()

    lazy var openSearch = {
        ComposeTransformer {
            $0.flatMap { (action: Home.SearchClick) -> Observable<HomeResult> in
                self.homeRouter.openSearch()
                        .andThen(Observable.empty())
            }
        }
    }()
}
