//
//  DropPinRouter.swift
//  MyStreetPlaces
//
//  Created by Oleksii.Kuropiatnyk on 13/04/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import iOSMvi

protocol DropPinRouter {
    func dismiss() -> Completable

    func viewPin(id: String) -> Completable
}

class DropPinRouterImpl: DropPinRouter {

    private var navigator: Navigator? {
        get {
            let name = HomeViewController.getNibName()
            return navigators[name]
        }
    }

    func dismiss() -> Completable {
        return Completable.create { observer in
            self.navigator?.navigate(event: NavigatorEvent.Pop(result: nil))
            observer(.completed)
            return Disposables.create()
        }
    }

    func viewPin(id: String) -> Completable {
        return Completable.create { subscriber in
            let viewPinVC = ViewPinController(nibName: ViewPinController.getNibName(), bundle: nil)
            viewPinVC.args[ViewPinController.PIN_ID_KEY] = id
            self.navigator?.navigate(event: NavigatorEvent.PresentVC(viewController: viewPinVC))
            subscriber(.completed)
            return Disposables.create()
        }
    }
}
