//
//  HomeAR.swift
//  MyStreetPlaces
//
//  Created by Катяя Куропятник on 02.04.18.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import GoogleMaps
import iOSMvi

protocol HomeAction {}
protocol HomeResult {}

struct Home{
    
    //MARK: Actions
    
    struct Initial : HomeAction {}
    struct DropClick : HomeAction {}
    struct ReloadPins : HomeAction {}
    struct LoadPins : HomeAction { let bounds: [CLLocation] }
    struct MapCenterUpdate : HomeAction{ let center: CLLocation }
    struct OpenPinDetails : HomeAction { let pinId: String }
    struct DropWithLocation : HomeAction { let location: CLLocation }
    struct SearchClick : HomeAction { }

    //MARK: result

    struct ShowmapCenterUnknownResult : HomeResult {}
    struct NoResult : HomeResult {}
    struct PinsLoadedResult : HomeResult { let pins: [Pin]? }
    struct FosucTo : HomeResult { let location: CLLocation }

    //MARK: ViewModel
    
    struct ViewModel {
        let pins: [Pin]?
        let showMapCenterUnknownDialog: OneShot?
        let focusTo: CLLocation?
    }

}
