//
// Created by Alexey Kuropiantnyk on 03/06/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation

struct Endpoints {
    static let BACKEND_URL = "https://fast-oasis-97329.herokuapp.com/"
}