//
//  AppAssemply.swift
//  MyStreetPlaces
//
//  Created by Oleksii.Kuropiatnyk on 03/04/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import DITranquillity
import CoreData
import RxSwift
import iOSMvi

var navigators: [String: Navigator] = [:]

func buildComponent(context: NSManagedObjectContext) -> DIContainer {

    let container = DIContainer()
    container.register { context }
            .lifetime(.perRun(.strong))

    container.register { ConcurrentDispatchQueueScheduler(qos: DispatchQoS.background) }
            .as(SchedulerType.self)
            .lifetime(.perRun(.strong))

    //MARK: Repos

    container.register1(PinsRepoImpl.init)
            .as(PinsRepo.self)
            .lifetime(.perRun(.strong))

    container.register(MapCenterRepoImpl.init)
            .as(MapCenterRepo.self)
            .lifetime(.perRun(.strong))

    container.register(DropPinModeRepoImpl.init)
            .as(DropPinModeRepo.self)
            .lifetime(.perRun(.strong))

    container.register(EditPinRepoImpl.init)
            .as(EditPinRepo.self)
            .lifetime(.perRun(.strong))

    container.register(LatestProjectionRepoImpl.init)
            .as(LatestProjectionRepo.self)
            .lifetime(.perRun(.strong))

    container.register1(UserRepoImpl.init)
            .as(UserRepo.self)
            .lifetime(.perRun(.strong))

    container.register1(SettingsRepoImpl.init)
            .as(SettingsRepo.self)
            .lifetime(.perRun(.strong))
    
    container.register(RequestMapFocusRepoImpl.init)
            .as(RequestMapFocusRepo.self)
            .lifetime(.perRun(.strong))

    //MARK: Home screen

    container.register(HomeInteractor.init)
    container.register1(HomePresenter.init)
    container.register(HomeRouterImpl.init)
            .as(HomeRouter.self)

    //MARK: Drop screen

    container.register(DropPinInteractor.init)
    container.register1(AddressRequestUseCase.init)
    container.register1(DropPinPresenter.init)
    container.register(DropPinRouterImpl.init)
            .as(DropPinRouter.self)

    //MARK: View screen

    container.register(ViewPinInteractor.init)
    container.register1(ViewPinPresenter.init)
    container.register(ViewPinRouterImpl.init)
            .as(ViewPinRouter.self)

    //Search screen

    container.register(SearchInteractor.init)
    container.register1(SearchPresenter.init)
    container.register(SearchRouterImpl.init)
            .as(SearchRouter.self)
    container.register1(FocusToLocationUCImpl.init)
        .as(FocusToLocationUC.self)

    //OptionToolbar

    container.register(LoginOnBackendUseCase.init)
    container.register(LoginWithFacebookUseCase.init)
    container.register(LoginUseCase.init)
    container.register(OptionsToolbarInteractor.init)
    container.register1(OptionsToolbarPresenter.init)

    //Settings screen

    container.register(SettingsRouterImpl.init)
        .as(SettingsRouter.self)
    container.register(SettingsInteractor.init)
    container.register1(SettingsPresenter.init)

    return container
}

extension DIContainer {
    func registerNavigator(navigator: Navigator, key: String) {
        navigators[key] = navigator
    }
}
