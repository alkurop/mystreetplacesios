//
//  SearchAR.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 10/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation

protocol SearchAction {}
protocol SearchResult {}

struct Search {
    
    //Action
    
    struct QueryAction : SearchAction { let query: String }
    struct ViewPinAction : SearchAction { let pin: Pin }
    
    //Result
    
    struct Result : SearchResult { let pins: [Pin] }
    
    // View Model
    
    struct ViewModel { let searchResults: [Pin] }
}
