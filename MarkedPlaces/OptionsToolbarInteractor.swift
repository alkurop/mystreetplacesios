//
// Created by Alexey Kuropiantnyk on 29/05/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import iOSMvi
import RxSwift

class OptionsToolbarInteractor: Interactor {
    typealias Action = OptionsToolbarAction
    typealias Result = OptionsToolbarResult

    private let loginUseCase: LoginUseCase
    private let scheduler: SchedulerType
    private let userRepo: UserRepo
    private let currentScreenSubject = BehaviorSubject<String?>(value: nil)

    init(
            loginUseCase: LoginUseCase,
            scheduler: SchedulerType,
            userRepo: UserRepo
    ) {
        self.loginUseCase = loginUseCase
        self.scheduler = scheduler
        self.userRepo = userRepo
    }

    func provideProcessor() -> ComposeTransformer<OptionsToolbarAction, OptionsToolbarResult> {
        return ComposeTransformer { upstream in
            return upstream.observeOn(self.scheduler)
                    .compose(ComposeTransformer { upstream -> Observable<OptionsToolbarResult> in
                        return Observable.merge([
                            upstream.filter { $0 is OptionsToolbar.LoginAction }.map { $0 as! OptionsToolbar.LoginAction }
                                    .compose(self.loginFacebook),
                            upstream.filter { $0 is OptionsToolbar.InitialAction }.map { $0 as! OptionsToolbar.InitialAction }
                                    .compose(self.initial)
                        ])
                    })
        }
    }

    lazy var loginFacebook = {
        ComposeTransformer {
            $0.flatMap { (action: OptionsToolbar.LoginAction) -> Observable<OptionsToolbarResult> in
                return self.currentScreenSubject.take(1)
                        .flatMap { screenName in
                            self.loginUseCase.execute(viewControllerName: screenName!)
                                .andThen(Observable.empty())
                        }
            }
        }
    }()

    lazy var initial = {
        ComposeTransformer {
            $0.flatMap { (action: OptionsToolbar.InitialAction) -> Observable<OptionsToolbarResult> in
                self.currentScreenSubject.onNext(action.screenName)
                let userRequest = self.userRepo.observeUser().share()
                let noUserResult = userRequest.filter { $0 == nil }.map { _ in
                    OptionsToolbar.NoUserResult() as OptionsToolbarResult
                }
                let hasUserResult = userRequest.filter { $0 != nil }.map { _ in
                    OptionsToolbar.HasUserResult() as OptionsToolbarResult
                }
                return Observable.merge([noUserResult, hasUserResult])
            }
        }
    }()
}
