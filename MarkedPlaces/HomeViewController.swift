//
//  HomeViewController.swift
//  MyStreetPlaces
//
//  Created by Oleksii.Kuropiatnyk on 13/04/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import UIKit
import GoogleMaps
import RxSwift
import CoreData
import DITranquillity
import iOSMvi
import iOSLocationUtils
import Then
import AnimTypeLabel

class HomeViewController: MviViewController {
    static let OPEN_SEARCH_REQUEST_CODE = 28

    override class func getNibName() -> String { return "HomeView" }

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var toolbar: OptionsToolbarView!
    
    let locationManager = CLLocationManager()
    var presenter: HomePresenter!
    let actionsBus = PublishSubject<HomeAction>()
    let mapUpdateBus = PublishSubject<MapUpdate>()
    var compositeDisposable: CompositeDisposable!
    var clusterManager: GMUClusterManager?
    var rendererDelegate: ClusterRendererDelegate?
    var focused = false
    var markers: [String: IdClusterItem] = [:]

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        compositeDisposable = CompositeDisposable()
        initPresenter()
        initMaps()

        self.navigationItem.title = LocalizedString("home_screen_title")
        self.navigationItem.rightBarButtonItem =
                UIBarButtonItem(title: LocalizedString("drop"),
                        style: .plain, target: self, action: #selector(drop))

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(
                barButtonSystemItem: UIBarButtonSystemItem.search,
                target: self,
                action: #selector(search)
        )
        actionsBus.onNext(Home.ReloadPins())
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        toolbar.setViewControllerName(name: HomeViewController.getNibName())
    }

    @objc func drop() { actionsBus.onNext(Home.DropClick()) }

    @objc func search() { actionsBus.onNext(Home.SearchClick()) }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        compositeDisposable.dispose()
    }

    func focusToPin(pin: Pin) {
        focusToLocation(lat: pin.content.lat, lng: pin.content.lng)
    }
    
    func focusToLocation(lat: Double, lng: Double){
        let coordinate = CLLocationCoordinate2D(latitude:lat, longitude: lng)
        let camera = GMSCameraPosition(target: coordinate, zoom: 17, bearing: 0, viewingAngle: 0)
        mapView.animate(to: camera)
    }

    func showCannotDropPinDialog() {
        let alert = UIAlertController(
                title: LocalizedString("cannot_drop_pin"),
                message: LocalizedString("pin_location_not_defined"),
                preferredStyle: UIAlertControllerStyle.alert)

        alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func showClusterBottomSheet(pins: Array<Pin>) {
        
    }
}
