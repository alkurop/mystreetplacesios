//
//  FocusToLocationUC.swift
//  My Places
//
//  Created by Alex on 25/07/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift

protocol FocusToLocationUC {
    func execute(pin: Pin) -> Completable
}

class FocusToLocationUCImpl : FocusToLocationUC {
    let focusRepo: RequestMapFocusRepo
    
    init(focusRepo: RequestMapFocusRepo) {
        self.focusRepo = focusRepo
    }
    
    func execute(pin: Pin) -> Completable {
        let location = CLLocation(latitude: pin.content.lat, longitude: pin.content.lng)
        return self.focusRepo.update(center: location)
    }
}
