//
//  IdMapMarler.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 08/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import GoogleMaps

class IdClusterItem: NSObject, GMUClusterItem {
    let pin: Pin
    let category: Category
    let title: String
    let position: CLLocationCoordinate2D
    
    var id : String {
        get { return pin.id }
    }

    init(pin: Pin) {
        self.pin = pin
        self.category = getCategoryByName(name: pin.content.categoryId)
        self.title = pin.content.title
        self.position = CLLocationCoordinate2D(latitude: pin.content.lat, longitude: pin.content.lng)
        super.init()
    }

    static public func ==(lhs: IdClusterItem, rhs: IdClusterItem) -> Bool {
        return lhs.isEqual(rhs) &&
                lhs.pin.id == rhs.pin.id &&
                lhs.category == rhs.category &&
                lhs.position == rhs.position
    }

    func getIconView () -> UIImageView {
        let frame = CGRect(0, 0, 40, 40);
        let imageView = UIImageView(frame: frame)
        let image = UIImage(named: category.icon)
        imageView.image = image
        return imageView
    }
}

extension CLLocationCoordinate2D: Equatable {
    static public func ==(left: CLLocationCoordinate2D, right: CLLocationCoordinate2D) -> Bool {
        return left.latitude == right.latitude && left.longitude == right.longitude
    }
}
