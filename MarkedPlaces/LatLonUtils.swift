//
//  LatLonUtils.swift
//  MyStreetPlaces
//
//  Created by Oleksii.Kuropiatnyk on 11/04/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import GoogleMaps
import iOSLocationUtils


func moveAlongBearing(location: CLLocation, bearing: Double, distanceMeters: Int) -> CLLocation {
    let R = 6378140.0
    let d = Double(distanceMeters)

    let lat1 = Measurement(value: location.coordinate.latitude, unit: UnitAngle.degrees).converted(to: .radians).value
    let lon1 = Measurement(value: location.coordinate.longitude, unit: UnitAngle.degrees).converted(to: .radians).value
    let brng = Measurement(value: bearing, unit: UnitAngle.degrees).converted(to: .radians).value
    let lat2 = asin(sin(lat1) * cos(d / R) + cos(lat1) * sin(d / R) * cos(brng))

    let lon2 = lon1 + atan2(sin(brng) * sin(d / R) * cos(lat1), cos(d / R) - sin(lat1) * sin(lat2))

    let degreesLat = Measurement(value: lat2, unit: UnitAngle.radians).converted(to: .degrees).value
    let degreesLon = Measurement(value: lon2, unit: UnitAngle.radians).converted(to: .degrees).value

    return CLLocation(latitude: degreesLat, longitude: degreesLon)

}

func getSquareOfDistanceMeters(_ location: CLLocation, _ distanceMeters: Int) -> [CLLocation] {
    let topBearing = 0.0
    let bottomBearing = 180.0
    let leftBearing = 90.0
    let rightBearing = 270.0

    let halfDistance = distanceMeters / 2

    let topPoint = moveAlongBearing(location: location, bearing: topBearing, distanceMeters: halfDistance)
    let bottomPoint = moveAlongBearing(location: location, bearing: bottomBearing, distanceMeters: halfDistance)
    let leftPoint = moveAlongBearing(location: location, bearing: leftBearing, distanceMeters: halfDistance)
    let rightPoint = moveAlongBearing(location: location, bearing: rightBearing, distanceMeters: halfDistance)

    let minPoint = CLLocation(latitude: bottomPoint.coordinate.latitude, longitude: rightPoint.coordinate.longitude)
    let maxPoint = CLLocation(latitude: topPoint.coordinate.latitude, longitude: leftPoint.coordinate.longitude)

    return [minPoint, maxPoint]
}

func getBounds(location: CLLocation, distanceMeters: Int) -> LocationBounds {
    let squareOfDistanceMeters = getSquareOfDistanceMeters(location, distanceMeters)
    return LocationBounds(southwest: squareOfDistanceMeters[0], northeast: squareOfDistanceMeters[1])
}

struct LocationBounds {
    let southwest: CLLocation
    let northeast: CLLocation
}

extension CLLocation {
    func toULocation() -> ULocation {
        return ULocation(lat: self.coordinate.latitude, lng: self.coordinate.longitude)
    }
}

extension CLLocationCoordinate2D {
    func toCLLocation() -> CLLocation {
        return CLLocation(latitude: self.latitude, longitude: self.longitude)
    }
}
