//
// Created by Alexey Kuropiantnyk on 25/05/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import iOSMvi

class SharePinUseCase {
    class func execute(pin: Pin, screenName: String) -> Completable {
        return Completable.create { sub in
                    var shareText = ""
                    let headerLabel = LocalizedString("sharing_a_place")
                    let titleLabel = LocalizedString("title")
                    let addressLabel = LocalizedString("address")
                    let descriptionLabel = LocalizedString("description")
                    let categoryLabel = LocalizedString("category_label_name")
                    let navigateLabel = LocalizedString("navigate")

                    let categoryName = getCategoryByName(name: pin.content.categoryId).name

                    shareText.append("\(headerLabel):\n\n")
                    shareText.append("\(titleLabel): \(pin.content.title)\n\n")
                    shareText.append("\(categoryLabel): \(categoryName)\n\n")

                    if !pin.content.address.isEmpty() {
                        shareText.append("\(addressLabel): \(pin.content.address!)\n\n")
                    }

                    if !pin.content.desc.isEmpty() {
                        shareText.append("\(descriptionLabel): \(pin.content.desc!)\n\n")
                    }

                    let mapUrl = GoogleMapWebUseCase.createMapUrl(pin: pin)
                    shareText.append("\(navigateLabel): \(mapUrl)")

                    let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
                    let navigator = getNavigatorByScreenName(screenName: screenName)
                    navigator.parent!.present(vc, animated: true)
                    return Disposables.create()
                }
                .subscribeOn(MainScheduler.instance)
    }

    private class func getNavigatorByScreenName(screenName: String) -> Navigator {
        return navigators[screenName]!
    }
}
