//
//  CategoryPickerDelegate.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 17/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import UIKit

extension DropPinViewController: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categoryCollection.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categoryCollection[row].name
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       categoryWasSelected(category: categoryCollection[row])
    }
    
    
    func showCategoryPicker(pin: Pin){
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            showPickerIphone(pin: pin)
        default:
            showPickerIpad(pin: pin)
        }
    }
    
    func showPickerIpad(pin: Pin){
        let title = LocalizedString("categry_select_label").appending("\n\n\n\n\n\n\n\n")
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: LocalizedString("ok"), style: .cancel, handler: { [unowned self] _ in self.pickerController = nil })
        alertController.addAction(cancelAction)
        
        let rect = CGRect(x: 10, y: 30, width: 260, height: 160)
        let pickerView = UIPickerView(frame: rect)
        
        let category = getCategoryByName(name: pin.content.categoryId)
        let defaultValue = categoryCollection.index(where: { $0.name == category.name })!
        alertController.view.addSubview(pickerView)
        
        pickerView.dataSource = self
        
        let centerX =  NSLayoutConstraint(
            item: pickerView,
            attribute: NSLayoutAttribute.centerX,
            relatedBy: .equal,
            toItem: alertController.view,
            attribute: NSLayoutAttribute.centerX,
            multiplier: 1,
            constant: 1
        )
        
        pickerView.selectRow(defaultValue, inComponent: 0, animated: false)
        pickerView.delegate = self
        alertController.view.addConstraint(centerX)
        
        self.present(alertController, animated: true, completion: { [unowned self] in
            self.pickerController = alertController
        })
    }
    
    func showPickerIphone(pin: Pin) {
        let title = LocalizedString("categry_select_label").appending("\n\n\n\n\n\n\n\n")
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: LocalizedString("ok"), style: .cancel, handler: { [unowned self] _ in self.pickerController = nil })
        alertController.addAction(cancelAction)
        
        let rect = CGRect(x: 10, y: 30, width: alertController.view.bounds.size.width - 10 * 4.0, height: 160)
        let pickerView = UIPickerView(frame: rect)
        
        let category = getCategoryByName(name: pin.content.categoryId)
        let defaultValue = categoryCollection.index(where: { $0.name == category.name })!
        alertController.view.addSubview(pickerView)

        pickerView.dataSource = self

        pickerView.selectRow(defaultValue, inComponent: 0, animated: false)
        pickerView.delegate = self
 
        self.present(alertController, animated: true, completion: { [unowned self] in
            self.pickerController = alertController
        })
    }
}

