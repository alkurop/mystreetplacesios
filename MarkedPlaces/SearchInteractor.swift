//
//  SearchInteractor.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 10/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import iOSMvi

class SearchInteractor: Interactor {

    typealias Action = SearchAction
    typealias Result = SearchResult

    let pinRepo: PinsRepo
    let scheduler: SchedulerType
    let searchRouter: SearchRouter
    let focusUseCase: FocusToLocationUC

    init(
            scheduler: SchedulerType,
            pinRepo: PinsRepo,
            searchRouter: SearchRouter,
            focusUseCase: FocusToLocationUC
    ) {
        self.scheduler = scheduler
        self.pinRepo = pinRepo
        self.searchRouter = searchRouter
        self.focusUseCase = focusUseCase
    }

    func provideProcessor() -> ComposeTransformer<SearchAction, SearchResult> {
        return ComposeTransformer { upstream in
            return upstream.observeOn(self.scheduler)
                    .compose(ComposeTransformer { upstream in
                        return Observable.merge([
                            upstream.filter { $0 is Search.QueryAction }.map { $0 as! Search.QueryAction }
                                    .compose(self.searchPins),
                            upstream.filter { $0 is Search.ViewPinAction }.map { $0 as! Search.ViewPinAction }
                                    .compose(self.showPin),
                        ])
                    })
        }
    }

    lazy var searchPins = {
        ComposeTransformer<Search.QueryAction, SearchResult> {
            $0.flatMap { (action: Search.QueryAction) -> Observable<SearchResult> in
                return self.pinRepo
                        .searchPins(query: action.query)
                        .map { pins in Search.Result(pins: pins) }
                        .asObservable()
            }
        }
    }()

    lazy var showPin = {
        ComposeTransformer<Search.ViewPinAction, SearchResult> {
            $0.flatMap { (action: Search.ViewPinAction) -> Observable<SearchResult> in
                return self.focusUseCase
                    .execute(pin: action.pin)
                    .andThen(self.searchRouter
                        .showPin(pin: action.pin)
                    )
                    .andThen(Observable.empty())
            }
        }
    }()
}
