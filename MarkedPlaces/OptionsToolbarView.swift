//
//  OptionsViewController.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 29/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import iOSMvi

class OptionsToolbarView: UIToolbar, UITabBarDelegate {
    private var presenter: OptionsToolbarPresenter!
    private let actionBus = PublishSubject<OptionsToolbarAction>()
    private var compositeDisposable = CompositeDisposable()
    private var screenName: String!

    override func didMoveToWindow() {
        if window != nil {
            onAttach()
        } else {
            onDetach()
        }
    }

    func onAttach() {
        initPresenter()
    }

    func onDetach() {
        compositeDisposable.dispose()
        compositeDisposable = CompositeDisposable()
    }

    func setViewControllerName(name: String) {
        screenName = name
        actionBus.onNext(OptionsToolbar.InitialAction(screenName: name))
    }

    func getViewController() -> MviViewController {
        return navigators[screenName]!.parent!
    }

    private func initPresenter() {
        let container = AppDelegate.container!
        presenter = container.resolve()

        let dis = presenter.subscribe(intentions: actionBus)
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { [unowned self] in self.renderView(state: $0) })
        let _ = compositeDisposable.insert(dis)
    }

    private func renderView(state: OptionsToolbar.ViewModel) {
        setIToolbarItems(shouldLogIn: state.shouldShowLoginButton, hasSettings: false)
    }

    func setIToolbarItems(shouldLogIn: Bool, hasSettings: Bool) {
        var items: [UIBarButtonItem] = []
        items.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil))

        if (shouldLogIn) {
            items.append(UIBarButtonItem(title: LocalizedString("log_in"), style: UIBarButtonItemStyle.done, target: self, action:  #selector(onSyncClick)))
            items.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil))
        }
        if (hasSettings) {
            items.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.play, target: self, action: #selector(onSettingsClick)))
            items.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil))
        }
        self.setItems(items, animated: false)
    }

    @objc func onSyncClick() {
        let alert = UIAlertController(
                title: LocalizedString("login_to_sync_alert_title"),
                message: LocalizedString("login_to_sync_alert_message"),
                preferredStyle: .alert)

        alert.addAction(UIAlertAction(
                title: LocalizedString("cancel"),
                style: .cancel,
                handler: nil)
        )

        alert.addAction(UIAlertAction(
                title: LocalizedString("ok"),
                style: .default,
                handler: { _ in
                    self.actionBus.onNext(OptionsToolbar.LoginAction())
                })
        )
        getViewController().present(alert, animated: true)
    }

    @objc func onSettingsClick() {
        //todo
    }
}
