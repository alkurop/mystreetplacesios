//
// Created by Alexey Kuropiantnyk on 03/06/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import CoreData

protocol SettingsRepo {
}

class SettingsRepoImpl: SettingsRepo {
    private let context: NSManagedObjectContext

    init(context: NSManagedObjectContext) {
        self.context = context
    }
}
