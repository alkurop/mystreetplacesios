//
//  HomeClusterDelegate.swift
//  My Places
//
//  Created by Alex on 25/07/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import GoogleMaps
import iOSMvi
import RxSwift

extension HomeViewController: GMSMapViewDelegate, GMUClusterManagerDelegate {
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let region = mapView.projection.visibleRegion()
        
        let bounds = [region.farLeft, region.farRight, region.nearLeft, region.nearRight]
            .map { $0.toCLLocation() }
        
        let center = mapView.camera.target.toCLLocation()
        mapUpdateBus.onNext(MapUpdate(bounds: bounds, center: center))
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        let clusterItem = marker.userData as! IdClusterItem
        let action = Home.OpenPinDetails(pinId: clusterItem.id)
        actionsBus.onNext(action)
    }
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        let action = Home.DropWithLocation(location: coordinate.toCLLocation())
        actionsBus.onNext(action)
    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
                                                 zoom: mapView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        mapView.moveCamera(update)
        let clusterItems = (cluster.items as! Array<IdClusterItem>)
            .map {clusterItem in clusterItem.pin}
        showClusterBottomSheet(pins: clusterItems)
        return false
    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap clusterItem: GMUClusterItem) -> Bool {
        let newCamera = GMSCameraPosition.camera(withTarget: clusterItem.position,
                                                 zoom: mapView.camera.zoom)
        let update = GMSCameraUpdate.setCamera(newCamera)
        mapView.moveCamera(update)
        
        let castItem = clusterItem as! IdClusterItem
        let action = Home.OpenPinDetails(pinId: castItem.id)
        actionsBus.onNext(action)
        
        return true
    }
}

func getExcludedClusterItems(markers: [String: IdClusterItem], pins: [Pin]) -> [String: IdClusterItem] {
    return markers.filter { (id: String, marker: IdClusterItem) in
        let category = marker.category
        let pinThatMatchesThisMarker = pins.filter { pin in
            let idIsEqual = pin.id == id
            let categoryIsIqual = getCategoryByName(name: pin.content.categoryId) == category
            return idIsEqual && categoryIsIqual
        }
        return pinThatMatchesThisMarker.isEmpty
    }
}

func getIncludedClusterItems(markers: [String: IdClusterItem], pins: [Pin]) -> [String: IdClusterItem] {
    var map: [String: IdClusterItem] = [:]
    pins.filter { pin in markers[pin.id] == nil }
        .forEach { pin in
            let marker = IdClusterItem(pin: pin)
            map[pin.id] = marker
    }
    return map
}

struct MapUpdate {
    let bounds: [CLLocation]
    let center: CLLocation
}
