//
// Created by Alexey Kuropiantnyk on 03/06/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import iOSMvi
import RxSwift

class SettingsInteractor: Interactor {
    typealias Action = SettingsAction
    typealias Result = SettingsResult

    let settingsRepo: SettingsRepo
    let scheduler: SchedulerType
    let settingsRouter: SettingsRouter

    init(
            scheduler: SchedulerType,
            settingsRepo: SettingsRepo,
            settingsRouter: SettingsRouter
    ) {

        self.scheduler = scheduler
        self.settingsRepo = settingsRepo
        self.settingsRouter = settingsRouter
    }


    func provideProcessor() -> ComposeTransformer<SettingsAction, SettingsResult> {
        return ComposeTransformer {
            return $0.observeOn(self.scheduler)
                    .compose(ComposeTransformer { upstream in
                        return Observable.merge([

                        ])
                    })
        }
    }
}
