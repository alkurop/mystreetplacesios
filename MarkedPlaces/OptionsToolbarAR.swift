//
// Created by Alexey Kuropiantnyk on 29/05/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation

protocol OptionsToolbarAction {}
protocol OptionsToolbarResult {}

struct OptionsToolbar {

    //Actions
    struct InitialAction: OptionsToolbarAction { let screenName: String }
    struct LoginAction: OptionsToolbarAction {}
    // todo struct OpenSettingsAction : OptionsToolbarAction {}

    //Result
    struct HasUserResult : OptionsToolbarResult {}
    struct NoUserResult : OptionsToolbarResult {}
    //ViewModel

    struct ViewModel { let shouldShowLoginButton: Bool }
}
