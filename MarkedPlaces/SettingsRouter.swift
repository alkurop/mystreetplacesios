//
// Created by Alexey Kuropiantnyk on 03/06/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import iOSMvi

protocol SettingsRouter {
    func dismiss() -> Completable
}

class SettingsRouterImpl: SettingsRouter {

    private var navigator: Navigator? {
        get {
            let name = SettingsViewController.getNibName()
            return navigators[name]
        }
    }

    func dismiss() -> Completable {
        return Completable.create { observer in
            self.navigator?.navigate(event: NavigatorEvent.Pop(result: nil))
            observer(.completed)
            return Disposables.create()
        }
    }

}
