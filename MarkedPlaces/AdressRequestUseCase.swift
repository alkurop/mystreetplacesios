//
// Created by Alexey Kuropiantnyk on 23/05/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import GoogleMaps
import RxSwift

class AddressRequestUseCase {

    private let editPinRepo: EditPinRepo

    init(editPinRepo: EditPinRepo) {
        self.editPinRepo = editPinRepo
    }

    func updateAddressFromCoordinate(coordinate: CLLocationCoordinate2D) -> Completable {

        return getAddress(coordinate: coordinate)
                .asObservable()
                .flatMap { address -> Completable in
                    self.editPinRepo.updateAddress(address: address)
                }
                .asObservable()
                .ignoreElements()
                .catchError { error in
                    print(error.localizedDescription)
                    return Completable.empty()
                }
    }

    private func getAddress(coordinate: CLLocationCoordinate2D) -> Single<String?> {
        return Single
                .create { sub in
                    let geocoder = GMSGeocoder()
                    geocoder.reverseGeocodeCoordinate(coordinate, completionHandler: {
                        result, error in
                        if let notNilError = error {
                            print(notNilError.localizedDescription)
                            sub(.error(notNilError))
                        }
                        if let notNilResult = result?.firstResult()?.lines {
                            sub(.success(notNilResult.joined(separator: ", ")))
                        } else {
                            sub(.success(nil))
                        }
                    })
                    return Disposables.create()
                }
                .subscribeOn(MainScheduler.instance)
    }
}
