//
//  DropPinInteractor.swift
//  MyStreetPlaces
//
//  Created by Oleksii.Kuropiatnyk on 13/04/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import iOSMvi

class DropPinInteractor: Interactor {
    typealias Action = DropPinAction
    typealias Result = DropPinResult

    private let scheduler: SchedulerType
    private let router: DropPinRouter
    private let modeRepo: DropPinModeRepo
    private let editPinRepo: EditPinRepo
    private let pinRepo: PinsRepo
    private let addressUseCase: AddressRequestUseCase

    init(scheduler: SchedulerType,
         router: DropPinRouter,
         modeRepo: DropPinModeRepo,
         editPinRepo: EditPinRepo,
         pinRepo: PinsRepo,
         addressUseCase: AddressRequestUseCase) {

        self.scheduler = scheduler
        self.router = router
        self.modeRepo = modeRepo
        self.editPinRepo = editPinRepo
        self.pinRepo = pinRepo
        self.addressUseCase = addressUseCase
    }

    func provideProcessor() -> ComposeTransformer<DropPinAction, DropPinResult> {
        return ComposeTransformer { upstream in
            return upstream.observeOn(self.scheduler)
                    .compose(ComposeTransformer { upstream in
                        return Observable.merge([
                            upstream.filter { $0 is DropPin.InitialActionWithLocation }.map { $0 as! DropPin.InitialActionWithLocation }
                                    .compose(self.initialLocation),
                            upstream.filter { $0 is DropPin.BackClick }.map { $0 as! DropPin.BackClick }
                                    .compose(self.backClick),
                            upstream.filter { $0 is DropPin.SubmitClick }.map { $0 as! DropPin.SubmitClick }
                                    .compose(self.submitClick),
                            upstream.filter { $0 is DropPin.DeleteClick }.map { $0 as! DropPin.DeleteClick }
                                    .compose(self.deleteClick),
                            upstream.filter { $0 is DropPin.DescriptionUpdated }.map { $0 as! DropPin.DescriptionUpdated }
                                    .compose(self.descriptionUpdated),
                            upstream.filter { $0 is DropPin.TitleUpdated }.map { $0 as! DropPin.TitleUpdated }
                                    .compose(self.titleUpdated),
                            upstream.filter { $0 is DropPin.InitialActionWithPinId }.map { $0 as! DropPin.InitialActionWithPinId }
                                    .compose(self.initialPinId),
                            upstream.filter { $0 is DropPin.CategoryUpdated }.map { $0 as! DropPin.CategoryUpdated }
                                    .compose(self.categoryUpdated),
                            upstream.filter { $0 is DropPin.AddressUpdated }.map { $0 as! DropPin.AddressUpdated }
                                    .compose(self.addressUpdated),
                            upstream.filter { $0 is DropPin.ShowCategoryPicker }.map { $0 as! DropPin.ShowCategoryPicker }
                                    .compose(self.showCategoryPicker),
                        ])
                    })
        }
    }

    lazy var initialPinId = {
        ComposeTransformer<DropPin.InitialActionWithPinId, DropPinResult> {
            $0.flatMap { (action: DropPin.InitialActionWithPinId) -> Observable<DropPinResult> in
                Observable.merge(

                        self.modeRepo.set(mode: DropPinMode.Editing)
                                .andThen(Observable.empty()),

                        self.modeRepo.observe()
                                .take(1)
                                .map { DropPin.ModeResult(mode: $0) },

                        self.pinRepo.getPinDetails(id: action.pinId)
                                .asObservable()
                                .flatMap { pin in
                                    self.editPinRepo.setPin(pin: pin!)
                                            .andThen(Observable.just(DropPin.PinLoaded(pin: pin!)))
                                }
                )
            }
        }
    }()

    lazy var initialLocation = {
        ComposeTransformer<DropPin.InitialActionWithLocation, DropPinResult> {
            $0.flatMap { (action: DropPin.InitialActionWithLocation) -> Observable<DropPinResult> in
                Observable.merge(
                        self.modeRepo.set(mode: DropPinMode.Adding)
                                .andThen(Observable.empty()),

                        self.editPinRepo.createPinWithLocation(location: action.location)
                                .andThen(Observable.empty()),

                        self.modeRepo.observe().take(1)
                                .map { DropPin.ModeResult(mode: $0) },
                        self.addressUseCase.updateAddressFromCoordinate(coordinate: action.location.coordinate)
                                .andThen(self.editPinRepo.getResultPin()).asObservable()
                                .map { pin in DropPin.PinLoaded(pin: pin) }
                )
            }
        }
    }()

    lazy var titleUpdated = {
        ComposeTransformer<DropPin.TitleUpdated, DropPinResult> {
            $0.flatMap { action in
                self.editPinRepo
                        .updateTitle(title: action.title)
                        .andThen(Observable.empty())
            }
        }
    }()

    lazy var categoryUpdated = {
        ComposeTransformer<DropPin.CategoryUpdated, DropPinResult> {
            $0.flatMap { action in
                self.editPinRepo
                        .updateCategory(category: action.category)
                        .andThen(self.editPinRepo
                                .getResultPin()
                                .asObservable()
                                .flatMap { pin -> Observable<DropPinResult> in
                                    Observable.just(DropPin.PinLoaded(pin: pin))
                                })
            }
        }
    }()

    lazy var addressUpdated = {
        ComposeTransformer<DropPin.AddressUpdated, DropPinResult> {
            $0.flatMap { action in
                self.editPinRepo
                        .updateAddress(address: action.address)
                        .andThen(Observable.empty())
            }
        }
    }()

    lazy var descriptionUpdated = {
        ComposeTransformer<DropPin.DescriptionUpdated, DropPinResult> {
            $0.flatMap { action in
                self.editPinRepo
                        .updateDescription(description: action.description)
                        .andThen(Observable.empty())
            }
        }
    }()

    lazy var backClick = {
        ComposeTransformer<DropPin.BackClick, DropPinResult> {
            $0.flatMap { (_: DropPin.BackClick) -> Observable<DropPinResult> in
                self.router.dismiss()
                        .andThen(Observable.empty())
            }
        }
    }()

    lazy var submitClick = {
        ComposeTransformer<DropPin.SubmitClick, DropPinResult> {
            $0.flatMap { _ in
                self.editPinRepo.getResultPin()
                        .asObservable()
                        .flatMap { pin -> Observable<DropPinResult> in
                            if pin.content.title.isEmpty {
                                return Observable.just(DropPin.NoTitleResult())
                            } else {
                                return self.modeRepo
                                        .observe()
                                        .take(1)
                                        .flatMap { (mode: DropPinMode) -> Observable<DropPinResult> in
                                            switch (mode) {
                                                case .Adding:
                                                    return self.pinRepo.addOrUpdatePin(pin: pin)
                                                            .andThen(self.router.dismiss())
                                                            .andThen(Observable.empty())
                                                case .Editing:
                                                    return self.pinRepo.addOrUpdatePin(pin: pin)
                                                            .andThen(self.router.dismiss())
                                                            .andThen(Observable.empty())
                                            }
                                        }
                            }
                        }
            }
        }
    }()

    lazy var deleteClick = {
        ComposeTransformer<DropPin.DeleteClick, DropPinResult> {
            $0.flatMap { _ in
                self.editPinRepo.getResultPin()
                        .asObservable()
                        .flatMap { pin -> Observable<DropPinResult> in
                            self.pinRepo.removePin(pinId: pin.id)
                                    .andThen(self.router.dismiss())
                                    .andThen(Observable.empty())
                        }
            }
        }
    }()

    lazy var showCategoryPicker = {
        ComposeTransformer<DropPin.ShowCategoryPicker, DropPinResult> {
            $0.flatMap { _ in
                self.editPinRepo
                        .getResultPin()
                        .asObservable()
                        .flatMap { (pin: Pin) -> Observable<DropPinResult> in
                            return Observable.just(DropPin.ShowCategoryPickerResult(pin: pin))
                        }

            }
        }
    }()
}

