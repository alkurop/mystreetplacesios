//
//  SearchPresenter.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 10/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import iOSMvi
import RxSwift

class SearchPresenter: Presenter {

    typealias Action = SearchAction
    typealias Result = SearchResult
    typealias ViewState = Search.ViewModel
    typealias Interactor1 = SearchInteractor

    var interactor: SearchInteractor

    init(interactor: SearchInteractor) {
        self.interactor = interactor
    }

    func resultToViewStateMapper() -> (Search.ViewModel, SearchResult) -> (Search.ViewModel) {
        return { oldModel, result in
            let pins = (result as! Search.Result).pins
            return Search.ViewModel(searchResults: pins)
        }
    }

    func defaultViewState() -> Search.ViewModel {
        return Search.ViewModel(searchResults: [])
    }
}
