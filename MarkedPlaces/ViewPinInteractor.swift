//
//  ViewPinInteractor.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 09/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import iOSMvi

class ViewPinInteractor: Interactor {

    typealias Action = ViewPinAction
    typealias Result = ViewPinResult

    let scheduler: SchedulerType
    let pinRepo: PinsRepo
    let router: ViewPinRouter
    let currentPin = BehaviorSubject<Optional<Pin>>.init(value: Optional.none)

    init(
            scheduler: SchedulerType,
            pinRepo: PinsRepo,
            router: ViewPinRouter
    ) {
        self.scheduler = scheduler
        self.pinRepo = pinRepo
        self.router = router
    }

    func provideProcessor() -> ComposeTransformer<ViewPinAction, ViewPinResult> {
        return ComposeTransformer { upstream in
            return upstream.observeOn(self.scheduler)
                    .compose(ComposeTransformer { upstream in
                        return Observable.merge([
                            upstream.filter { $0 is ViewPin.Edit }.map { $0 as! ViewPin.Edit }
                                    .compose(self.editPin),
                            upstream.filter { $0 is ViewPin.Load }.map { $0 as! ViewPin.Load }
                                    .compose(self.loadPin),
                            upstream.filter { $0 is ViewPin.Navigate }.map { $0 as! ViewPin.Navigate }
                                    .compose(self.navigate),
                            upstream.filter { $0 is ViewPin.Share }.map { $0 as! ViewPin.Share }
                                    .compose(self.share),
                        ])
                    })
        }
    }

    lazy var loadPin = {
        ComposeTransformer<ViewPin.Load, ViewPinResult> {
            $0.flatMap { action in
                return self.pinRepo.getPinDetails(id: action.pinId)
                        .asObservable()
                        .flatMap { (pin: Pin?) -> Observable<ViewPinResult> in
                            if let resultPin = pin {
                                return  Completable
                                        .create { sub in
                                            self.currentPin.onNext(resultPin)
                                            sub(.completed)
                                            return Disposables.create()
                                        }
                                        .andThen(Observable.just(ViewPin.LoadedResult(pin: resultPin)))
                            } else {
                                return  self.router.dismiss()
                                        .andThen(Observable.empty())
                            }
                        }
            }
        }
    }()

    lazy var editPin = {
        ComposeTransformer<ViewPin.Edit, ViewPinResult> {
            $0.flatMap { (action: ViewPin.Edit) -> Observable<ViewPinResult> in
                self.currentPin
                        .take(1)
                        .flatMap { (pin: Optional<Pin>) -> Observable<ViewPinResult> in
                            let castPin = pin!
                            return self.router
                                    .edit(pinId: castPin.id)
                                    .andThen(Observable.empty())
                        }
            }
        }
    }()

    lazy var navigate = {
        ComposeTransformer<ViewPin.Navigate, ViewPinResult> {
            $0.flatMap { (action: ViewPin.Navigate) -> Observable<ViewPinResult> in
                self.currentPin
                        .take(1)
                        .flatMap { (pin: Optional<Pin>) -> Observable<ViewPinResult> in
                            let castPin = pin!
                            return GoogleNavigatePinUseCase.execute(pin: castPin)
                                    .andThen(Observable.empty())
                        }
            }
        }
    }()

    lazy var share = {
        ComposeTransformer<ViewPin.Share, ViewPinResult> {
            $0.flatMap { (action: ViewPin.Share) -> Observable<ViewPinResult> in
                self.currentPin
                        .take(1)
                        .flatMap { (pin: Optional<Pin>) -> Observable<ViewPinResult> in
                            let castPin = pin!
                            return SharePinUseCase.execute(pin: castPin, screenName: action.screenName)
                                    .andThen(Observable.empty())
                        }
            }
        }
    }()
}
