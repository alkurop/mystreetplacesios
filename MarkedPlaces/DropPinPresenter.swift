//
//  DropPinPresenter.swift
//  MyStreetPlaces
//
//  Created by Oleksii.Kuropiatnyk on 13/04/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import iOSMvi

class DropPinPresenter: Presenter {

    typealias Action = DropPinAction
    typealias Result = DropPinResult
    typealias ViewState = DropPin.ViewModel
    typealias Interactor1 = DropPinInteractor

    var interactor: DropPinInteractor

    init(interactor: DropPinInteractor) {
        self.interactor = interactor
    }

    func resultToViewStateMapper() -> (DropPin.ViewModel, DropPinResult) -> (DropPin.ViewModel) {
        return { oldModel, result in
            var newModel = oldModel
            switch result {
                case is DropPin.PinLoaded:
                    newModel.pin = (result as! DropPin.PinLoaded).pin
                case is DropPin.ModeResult:
                    newModel.deleteButtonVisible = (result as! DropPin.ModeResult).mode == DropPinMode.Editing
                case is DropPin.NoTitleResult:
                    newModel.noTitleAlert = OneShot()
                case is DropPin.ShowCategoryPickerResult:
                    let pin = (result as! DropPin.ShowCategoryPickerResult).pin
                    newModel.showCategoryPicker = OneShot(pin)
                default:
                    break
            }
            return newModel
        }
    }

    func defaultViewState() -> DropPin.ViewModel {
        return DropPin.ViewModel(
                pin: nil,
                deleteButtonVisible: false,
                noTitleAlert: nil,
                showCategoryPicker: nil,
                shouldSetDefaultValues: OneShot()
        )
    }
}
