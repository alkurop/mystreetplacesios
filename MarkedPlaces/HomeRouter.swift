//
//  HomeRouter.swift
//  MyStreetPlaces
//
//  Created by Oleksii.Kuropiatnyk on 13/04/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import GoogleMaps
import RxSwift
import iOSMvi

protocol HomeRouter {

    func dropPin(location: CLLocation) -> Completable

    func viewPin(id: String) -> Completable

    func openSearch() -> Completable
}

class HomeRouterImpl: HomeRouter {

    private var navigator: Navigator? {
        get {
            let name = HomeViewController.getNibName()
            return navigators[name]
        }
    }

    func dropPin(location: CLLocation) -> Completable {
        return Completable.create { subscriber in
            let droppinVC = DropPinViewController(nibName: DropPinViewController.getNibName(), bundle: nil)
            droppinVC.args[DropPinViewController.LOCATION_KEY] = location
            self.navigator?.navigate(event: NavigatorEvent.PresentVC(viewController: droppinVC))
            subscriber(.completed)
            return Disposables.create()
        }
    }

    func viewPin(id: String) -> Completable {
        return Completable.create { subscriber in
            let viewPinVC = ViewPinController(nibName: ViewPinController.getNibName(), bundle: nil)
            viewPinVC.args[ViewPinController.PIN_ID_KEY] = id
            self.navigator?.navigate(event: NavigatorEvent.PresentVC(viewController: viewPinVC))
            subscriber(.completed)
            return Disposables.create()
        }
    }

    func openSearch() -> Completable {
        return Completable.create { subscriber in
            let searchVC = SearchViewController(nibName: SearchViewController.getNibName(), bundle: nil, requestId: HomeViewController.OPEN_SEARCH_REQUEST_CODE)

            let parent = self.navigator?.parent
            parent?.openOver(child: searchVC)

            subscriber(.completed)
            return Disposables.create()
        }
    }
}

extension UIViewController {
    func openOver(child: UIViewController) {
        DispatchQueue.main.sync {
            let navVC = UINavigationController(rootViewController: child)
            navVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(navVC, animated: false, completion: nil)
        }
    }
}
