//
//  ViewPinRouter.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 09/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import iOSMvi

protocol ViewPinRouter {
    func dismiss() -> Completable

    func edit(pinId: String) -> Completable
}

class ViewPinRouterImpl: ViewPinRouter {

    private var navigator: Navigator? {
        get {
            let name = ViewPinController.getNibName()
            return navigators[name]
        }
    }

    func dismiss() -> Completable {
        return Completable.create { observer in
            self.navigator?.navigate(event: NavigatorEvent.Pop(result: nil))
            observer(.completed)
            return Disposables.create()
        }
    }

    func edit(pinId: String) -> Completable {
        return Completable.create { sub in
            let editPinVC = DropPinViewController(nibName: DropPinViewController.getNibName(), bundle: nil)
            editPinVC.args[DropPinViewController.PIN_ID_KEY] = pinId
            self.navigator?.navigate(event: NavigatorEvent.PresentVC(viewController: editPinVC))
            sub(.completed)
            return Disposables.create()
        }
    }
}
