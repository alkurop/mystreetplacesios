//
//  Utils.swift
//  MyStreetPlaces
//
//  Created by Oleksii.Kuropiatnyk on 28/04/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import CoreGraphics
import UIKit

func LocalizedString(_ key: String) -> String {
    return NSLocalizedString(key, comment: "")
}

extension CGRect {
    init(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) {
        self.init(x: x, y: y, width: width, height: height)
    }
}

extension CGSize {
    init(_ width: CGFloat, _ height: CGFloat) {
        self.init(width: width, height: height)
    }
}

extension CGPoint {
    init(_ x: CGFloat, _ y: CGFloat) {
        self.init(x: x, y: y)
    }
}

extension Optional where Wrapped == String {
    func isEmpty() -> Bool {
        return (self ?? "").isEmpty
    }
}

func hintColor() -> UIColor {
    return UIColor.gray.withAlphaComponent(0.5)
}
