//
// Created by Alexey Kuropiantnyk on 29/05/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import CoreData

protocol UserRepo {
    func setUser(user: User) -> Completable
    func observeUser() -> Observable<User?>
}

class UserRepoImpl: UserRepo {
    private let context: NSManagedObjectContext
    private let userUpdatesPublisher = BehaviorSubject(value: 0)

    init(context: NSManagedObjectContext) {
        self.context = context
    }

    func setUser(user: User) -> Completable {
        return Completable.create { sub in
            let _ = user.toMo(context: self.context)
            do {
                try self.context.save()
                self.userUpdatesPublisher.onNext(0)
                sub(.completed)
            } catch {
                sub(.error(error))
            }
            return Disposables.create()
        }
    }

    func observeUser() -> Observable<User?> {
        return userUpdatesPublisher.flatMap { _ in
            return  Single.create { sub in
                        let request: NSFetchRequest<UserMO> = UserMO.fetchRequest()
                        do {
                            let result = try self.context
                                    .fetch(request).map { $0.toUser() }
                            if (result.count == 0) {
                                sub(.success(nil))

                            } else if (result.count == 1) {
                                sub(.success(result[0]))

                            } else {
                                //User repo holds more then 1 user
                                let error = NSError()
                                sub(.error(error))
                            }
                        } catch {
                            sub(.error(error))
                        }
                        return Disposables.create()
                    }
                    .asObservable()
        }
    }

}
