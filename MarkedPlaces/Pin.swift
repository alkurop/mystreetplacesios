//
//  Pin.swift
//  MyStreetPlaces
//
//  Created by Oleksii.Kuropiatnyk on 04/04/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import CoreData
import Then

struct Pin {
    var id: String
    var meta: Meta
    var content: Content

    struct Meta {
        var modified: Int64
        var ownerId: String
        var isSynced: Bool
    }

    struct Content {
        var title: String
        var address: String?
        var categoryId: String?
        var desc: String?
        var lat: Double
        var lng: Double
        var pictures: [PictureWrapper]
    }
}

extension PinMO {
    func toPin() -> Pin {
        let pics = self.pictures?.array.map { mo in
            (mo as! PictureWrapperMO).toPictureWrapper()
        } ?? []
        return Pin(
            id: self.id!,
            meta: Pin.Meta(modified: self.modified, ownerId: self.ownerId!, isSynced: self.isSynced),
            content: Pin.Content(
                title: self.title!,
                address: self.address,
                categoryId: self.categoryId,
                desc: self.desc,
                lat: self.lat,
                lng: self.lng,
                pictures: pics)
        )
    }
}

extension Pin: Then {
    func toMO(context: NSManagedObjectContext) -> PinMO {
        let pinMo = PinMO(context: context)
            .then { it in
                it.id = self.id
                it.title = self.content.title
                it.address = self.content.address
                it.categoryId = self.content.categoryId
                it.desc = self.content.desc
                it.isSynced = self.meta.isSynced
                it.lat = self.content.lat
                it.lng = self.content.lng
                it.ownerId = self.meta.ownerId
                it.modified = self.meta.modified
        }
        let pictures = self.content.pictures.map { $0.toMO(context: context, pin: pinMo) }
        pictures.forEach { picture in pinMo.addToPictures(picture) }
        return pinMo
    }
}

protocol Changeable {
}

extension Changeable {
    func changing(change: (inout Self) -> Void) -> Self {
        var a = self
        change(&a)
        return a
    }
}

extension Pin: Changeable {
}
