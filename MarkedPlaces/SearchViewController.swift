//
//  SearchViewController.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 10/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import iOSMvi

class SearchViewController: MviViewController {
    override class func getNibName() -> String {
        return "SearchView"
    }

    @IBOutlet weak var tableView: UITableView!
    private var presenter: SearchPresenter!
    private var compositeDisposable: CompositeDisposable!
    private let actionsBus = PublishSubject<SearchAction>()
    private var searchField: UISearchBar!
    private var navigator: Navigator!
    var pins: [Pin] = []

    override func viewWillAppear(_ animated: Bool) {
        configureTableView()
        initPresenter()
        configureBar()
        confivureDismissOnTouchOutside()
        setListeners()
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchField.becomeFirstResponder()
    }

    override func viewWillDisappear(_ animated: Bool) {
        compositeDisposable.dispose()
        super.viewWillAppear(animated)
    }

    func configureTableView() {
        tableView.register(
                UINib(nibName: SearchTableCell.getNibName(), bundle: nil),
                forCellReuseIdentifier: SearchTableCell.getIdentifier()
        )
        tableView.dataSource = self
    }

    func confivureDismissOnTouchOutside() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.back))
        self.view.addGestureRecognizer(tapGesture)
    }

    @objc func back() {
        dismiss(animated: false, completion: nil)
    }

    func configureBar() {
        searchField = UISearchBar()
        searchField.placeholder = LocalizedString("search_field_hint")
        self.navigationItem.titleView = searchField
        let backButton = UIBarButtonItem(
                image: UIImage(named: "back_arrow"),
                style: UIBarButtonItemStyle.done,
                target: self,
                action: #selector(back)
        )
        self.navigationItem.leftBarButtonItem = backButton
    }

    func setListeners() {
        let dis = searchField.rx.text.orEmpty
                .subscribe(onNext: {
            [unowned self] query in
            let action = Search.QueryAction(query: query)
            self.actionsBus.onNext(action)
        })
        let _ = compositeDisposable.insert(dis)
    }

    func initPresenter() {
        compositeDisposable = CompositeDisposable()
        let container = AppDelegate.container!
        navigator = Navigator(viewController: self)
        container.registerNavigator(navigator: navigator, key: SearchViewController.getNibName())

        presenter = container.resolve()
        let dis = presenter.subscribe(intentions: actionsBus)
                .observeOn(MainScheduler.instance)
                .subscribe { [unowned self] event in
                    if let model = event.element {
                        self.renderView(model: model)
                    }
                }
        let _ = compositeDisposable.insert(dis)
    }

    func renderView(model: Search.ViewModel) {
        pins = model.searchResults
        self.tableView.reloadData()
    }
}

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pins.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
                withIdentifier: SearchTableCell.getIdentifier(),
                for: indexPath) as! SearchTableCell

        let position = indexPath.row

        let pin = pins[position]
        cell.labelView.text = pin.content.title
        let category = getCategoryByName(name: pin.content.categoryId)
        cell.categotyView.image  = UIImage(named: category.icon)

        cell.clickListener = { index in
            let selectedPin = self.pins[index]
            let action = Search.ViewPinAction(pin: selectedPin)
            self.actionsBus.onNext(action)
        }

        return cell
    }
}


