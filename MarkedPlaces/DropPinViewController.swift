//
//  DropPinViewController.swift
//  MyStreetPlaces
//
//  Created by Oleksii.Kuropiatnyk on 13/04/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import UIKit
import iOSMvi
import RxSwift
import RxCocoa
import RxGesture
import CoreGraphics
import GoogleMaps
import UITextView_Placeholder
import AnimTypeLabel

class DropPinViewController: MviViewController {
    static let LOCATION_KEY = "location key"
    static let PIN_ID_KEY = "pin_id_key"

    @IBOutlet weak var categoryName: AnimTypeLabel!
    @IBOutlet weak var categoryLabel: LocalizableLabel!
    @IBOutlet weak var categoryIcon: UIImageView!
    @IBOutlet weak var titleInput: AnimTypeTextField!
    @IBOutlet weak var descriptionInput: AnimTypeTextView!
    @IBOutlet weak var titleLabel: LocalizableLabel!
    @IBOutlet weak var descLabel: LocalizableLabel!
    @IBOutlet weak var containerView: UIStackView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var addressLabel: LocalizableLabel!
    @IBOutlet weak var addressInput: AnimTypeTextField!
    @IBOutlet weak var deleteButtonWidth: NSLayoutConstraint!
    
    private var presenter: DropPinPresenter!
    private let actionsBus = PublishSubject<DropPinAction>()
    private var compositeDisposable = CompositeDisposable()
    var pickerController: UIAlertController? = nil

    override class func getNibName() -> String {
        return "DropView"
    }

    override func viewWillAppear(_ animated: Bool) {
        compositeDisposable = CompositeDisposable()
        configureInputs()
        configureMenus()

        configurePresenter()
        configureInputListeners()

        titleLabel.labelStyle()
        descLabel.labelStyle()
        categoryLabel.labelStyle()
        addressLabel.labelStyle()

        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        if let locationParam = args[DropPinViewController.LOCATION_KEY] as? CLLocation { actionsBus.onNext(DropPin.InitialActionWithLocation(location: locationParam)) }
        if let pinIdParam = args[DropPinViewController.PIN_ID_KEY] as? String { actionsBus.onNext(DropPin.InitialActionWithPinId(pinId: pinIdParam)) }
        super.viewDidAppear(animated)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        compositeDisposable.dispose()
    }

    func configurePresenter() {
        let container = AppDelegate.container!
        let navigation = Navigator(viewController: self)
        container.registerNavigator(navigator: navigation, key: DropPinViewController.getNibName())

        presenter = container.resolve()
        let dis = presenter.subscribe(intentions: actionsBus)
                .observeOn(MainScheduler.instance)
                .subscribe { [unowned self] event in
                    if let viewModel = event.element { self.renderView(viewModel) }
                }
        let _ = compositeDisposable.insert(dis)
    }

    func configureMenus() {
        let backButton = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(back))
        self.navigationItem.backBarButtonItem = backButton

        let acceptButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.save, target: self, action: #selector(save))
        self.navigationItem.rightBarButtonItem = acceptButton
    }

    func configureInputs() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard(_:)))
        self.view.addGestureRecognizer(tapGesture)
        categoryName.contentStyle()
        categoryName.text = nil

        titleInput.placeholder = LocalizedString("title_hint")
        titleInput.returnKeyType = UIReturnKeyType.done

        addressInput.placeholder = LocalizedString("address_hint")
        addressInput.returnKeyType = UIReturnKeyType.done

        descriptionInput.roundBorderStyle()
        descriptionInput.placeholder = LocalizedString("description_hint")

        deleteButton.setTitle(LocalizedString("delete"), for: UIControlState.normal)
        deleteButton.layer.borderWidth = 1
        deleteButton.layer.borderColor = UIColor.red.cgColor
        deleteButton.clipsToBounds = true
        deleteButton.alpha = 0.8
    }

    func configureInputListeners() {
        let sub1 = descriptionInput.rx.text.subscribe { [unowned self] text in
            if let string = text.element ?? "" { self.actionsBus.onNext(DropPin.DescriptionUpdated(description: string)) }
        }

        let sub2 = addressInput.rx.text.subscribe { text in
            if let elem = text.element ?? "" { self.actionsBus.onNext(DropPin.AddressUpdated(address: elem)) }
        }

        let sub3 = titleInput.rx.text.subscribe { text in
            if let elem = text.element ?? "" { self.actionsBus.onNext(DropPin.TitleUpdated(title: elem)) }
        }

        let sub4 = containerView.rx.tapGesture().when(.recognized)
                .subscribe(onNext: { [unowned self]_ in self.actionsBus.onNext(DropPin.ShowCategoryPicker()) })

        let sub5 = deleteButton.rx.tapGesture().when(.recognized)
                .subscribe(onNext: { [unowned self]_ in self.showDeleteConfirmation() })

        let _ = compositeDisposable.insert(sub1)
        let _ = compositeDisposable.insert(sub2)
        let _ = compositeDisposable.insert(sub3)
        let _ = compositeDisposable.insert(sub4)
        let _ = compositeDisposable.insert(sub5)
    }

    func showDeleteConfirmation() {
        let confirmation = UIAlertController(title: LocalizedString("delete_confirmation_title"),
                message: LocalizedString("delete_confirmation_msg"),
                preferredStyle: UIAlertControllerStyle.alert)

        let cancelAction = UIAlertAction(title: LocalizedString("cancel"), style: UIAlertActionStyle.cancel)
        let okAction = UIAlertAction(title: LocalizedString("yes"), style: UIAlertActionStyle.default,
                handler: { _ in self.actionsBus.onNext(DropPin.DeleteClick()) })

        confirmation.addAction(okAction)
        confirmation.addAction(cancelAction)
        self.present(confirmation, animated: true)
    }

    func categoryWasSelected(category: Category) {
        actionsBus.onNext(DropPin.CategoryUpdated(category: category))
    }

    @objc func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        descriptionInput.resignFirstResponder()
        titleInput.resignFirstResponder()
    }

    @objc func back() { actionsBus.onNext(DropPin.BackClick()) }

    @objc func save() { actionsBus.onNext(DropPin.SubmitClick()) }

    func renderView(_ viewModel: DropPin.ViewModel) {
        if viewModel.shouldSetDefaultValues.isActive() {
            if viewModel.deleteButtonVisible {
                self.navigationItem.title = LocalizedString("edit_pin")
                deleteButton.constraints.filter({ $0.firstAttribute == NSLayoutAttribute.height })
                        .forEach { $0.constant = 30 }
            } else {
                self.navigationItem.title = LocalizedString("drop_pin")
            }
        }

        if let pin = viewModel.pin {
            titleInput.setAnimatedText(text: pin.content.title)
            if !(pin.content.desc?.isEmpty ?? true) {
                descriptionInput.setAnimatedText(text: pin.content.desc, delayMicros: 40)
            }
            let category = getCategoryByName(name: pin.content.categoryId)
            categoryName.setAnimatedText(text: category.name)
            categoryIcon.image = UIImage(named: category.icon)
            addressInput.setAnimatedText(text: pin.content.address)
        }

        if viewModel.noTitleAlert?.isActive() ?? false {
            let alert = UIAlertController(
                    title: LocalizedString("no_title_alert_title"),
                    message: LocalizedString("no_title_alert_msg"),
                    preferredStyle: .alert
            )

            alert.addAction(UIAlertAction(
                    title: LocalizedString("ok"),
                    style: .cancel,
                    handler: nil
            ))

            self.present(alert, animated: true)
        }

        if let showCategoryPicker = viewModel.showCategoryPicker {
            if showCategoryPicker.isActive() {
                let pin = showCategoryPicker.params as? Pin
                self.showCategoryPicker(pin: pin!)
            }
        }
        if viewModel.deleteButtonVisible {
            deleteButtonWidth.constant = 100
        } else{
            deleteButtonWidth.constant = 0
        }
    }
}
