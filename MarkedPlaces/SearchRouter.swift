//
//  SearchRouter.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 10/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import iOSMvi

protocol SearchRouter {
    func showPin(pin: Pin) -> Completable
}

class SearchRouterImpl: SearchRouter {

    private var navigator: Navigator? {
        get {
            let name = SearchViewController.getNibName()
            return navigators[name]
        }
    }

    func showPin(pin: Pin) -> Completable {
        return openViewPinScreen(pin: pin)
    }
    
    private func openViewPinScreen(pin: Pin) -> Completable {
        return Completable.create { sub in
            let viewPinVC = ViewPinController(nibName: ViewPinController.getNibName(), bundle: nil)
            viewPinVC.args[ViewPinController.PIN_ID_KEY] = pin.id
            self.navigator?.navigate(event: NavigatorEvent.PresentVC(viewController: viewPinVC))
            sub(.completed)
            return Disposables.create()
        }
    }
}
