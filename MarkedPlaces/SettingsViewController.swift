//
// Created by Alexey Kuropiantnyk on 03/06/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import iOSMvi

class SettingsViewController: MviViewController {

    override class func getNibName() -> String {
        return "SettingsView"
    }
}
