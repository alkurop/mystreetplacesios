//
//  PictureWrapper.swift
//  MyStreetPlaces
//
//  Created by Oleksii.Kuropiatnyk on 04/04/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import CoreData
import Then

struct PictureWrapper {
    let id: String
    let meta: Meta
    let content: Content

    struct Meta {
        let created: Int64
        let pinId: String
    }

    struct Content {
        let localPhoto: String?
        let serverPhoto: String?
    }
}

extension PictureWrapperMO {
    func toPictureWrapper() -> PictureWrapper {
        return PictureWrapper(
                id: self.id!,
                meta: PictureWrapper.Meta(created: self.timeStamp, pinId: self.parent!.id!),
                content: PictureWrapper.Content(localPhoto: self.localPhoto, serverPhoto: self.serverPhoto)
        )
    }
}

extension PictureWrapper: Then {
    func toMO(context: NSManagedObjectContext, pin: PinMO) -> PictureWrapperMO {
        let wrapper = PictureWrapperMO(context: context).then { it in
            it.id = self.id
            it.localPhoto = self.content.localPhoto
            it.serverPhoto = self.content.serverPhoto
            it.timeStamp = self.meta.created
            it.parent = pin
        }
        return wrapper
    }
}
