//
//  HomePresenter.swift
//  MyStreetPlaces
//
//  Created by Oleksii.Kuropiatnyk on 11/04/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import iOSMvi

class HomePresenter: Presenter {

    typealias Action = HomeAction
    typealias Result = HomeResult
    typealias ViewState = Home.ViewModel
    typealias Interactor1 = HomeInteractor

    var interactor: HomeInteractor

    init(interactor: HomeInteractor) {
        self.interactor = interactor
    }

    func resultToViewStateMapper() -> (ViewState, Result) -> (ViewState) {
        return { oldModel, result in
            let viewModel: ViewState
            switch result {
                case is Home.PinsLoadedResult:
                    viewModel = ViewState(pins: (result as! Home.PinsLoadedResult).pins, showMapCenterUnknownDialog: nil, focusTo: nil)
                case is Home.ShowmapCenterUnknownResult:
                    viewModel = ViewState(pins: oldModel.pins, showMapCenterUnknownDialog: OneShot(), focusTo: nil)
                case is Home.FosucTo:
                    viewModel = ViewState(pins: oldModel.pins, showMapCenterUnknownDialog: nil, focusTo: (result as! Home.FosucTo).location)

                default:
                    viewModel = ViewState(pins: oldModel.pins, showMapCenterUnknownDialog: nil, focusTo: nil)
            }
            return viewModel
        }
    }

    func defaultViewState() -> ViewState {
        return ViewState(pins: nil, showMapCenterUnknownDialog: nil, focusTo: nil)
    }
}
