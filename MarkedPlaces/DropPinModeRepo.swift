//
//  DropPinModeRepo.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 04/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift

protocol DropPinModeRepo {

    func set(mode: DropPinMode) -> Completable
    func observe() -> Observable<DropPinMode>
}

class DropPinModeRepoImpl: DropPinModeRepo {
    private let publisher = BehaviorSubject(value: DropPinMode.Adding)

    func set(mode: DropPinMode) -> Completable {
        return Completable.create { sub in
            self.publisher.onNext(mode)
            sub(.completed)
            return Disposables.create()
        }
    }

    func observe() -> Observable<DropPinMode> {
        return publisher
    }
}

enum DropPinMode {
    case Adding
    case Editing
}
