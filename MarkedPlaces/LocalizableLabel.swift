//
// Created by Alexey Kuropiantnyk on 24/05/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation

@IBDesignable
class LocalizableLabel: UILabel {

    @IBInspectable var localisedKey: String? {
        didSet {
            guard let key = localisedKey else { return }
            text = NSLocalizedString(key, comment: "")
        }
    }
}
