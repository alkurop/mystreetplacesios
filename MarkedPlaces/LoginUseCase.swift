//
// Created by Alexey Kuropiantnyk on 29/05/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift

class LoginUseCase {

    private let loginFB: LoginWithFacebookUseCase
    private let loginBE: LoginOnBackendUseCase

    init(loginFB: LoginWithFacebookUseCase, loginBE: LoginOnBackendUseCase) {
        self.loginFB = loginFB
        self.loginBE = loginBE
    }

    func execute(viewControllerName: String) -> Completable {
        return loginFB
                .execute(viewControllerName: viewControllerName)
                .asObservable()
                .flatMap { result in
                    self.loginBE.execute(token: result.token)
                            .asObservable()
                }
                .ignoreElements()
    }
}
