//
//  StorageImpl.swift
//  MyStreetPlaces
//
//  Created by Oleksii.Kuropiatnyk on 04/04/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import CoreData

class StorageImpl {
    let managedObjectContext: NSManagedObjectContext

    init(completionClosure: @escaping (NSManagedObjectContext) -> ()) {
        guard let modelURL = Bundle.main.url(forResource: "MyStreetPlaces", withExtension: "momd") else {
            fatalError("Error loading model from bundle")
        }
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Error initializing mom from: \(modelURL)")
        }

        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)

        managedObjectContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
        managedObjectContext.mergePolicy = NSOverwriteMergePolicy
        guard let docURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last else {
            fatalError("Unable to resolve document directory")
        }
        let storeURL = docURL.appendingPathComponent("MyStreetPlaces.sqlite")
        do {
            try persistentStoreCoordinator.addPersistentStore(
                    ofType: NSSQLiteStoreType,
                    configurationName: nil,
                    at: storeURL,
                    options: nil
            )
            completionClosure(self.managedObjectContext)

        } catch {
            fatalError("Error migrating store: \(error)")
        }

    }
}
