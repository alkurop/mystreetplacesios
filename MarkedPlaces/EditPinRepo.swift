//
//  EditPinRepo.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 04/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import GoogleMaps

protocol EditPinRepo {

    func setPin(pin: Pin) -> Completable

    func createPinWithLocation(location: CLLocation) -> Completable

    func updateTitle(title: String) -> Completable

    func updateDescription(description: String) -> Completable

    func updateCategory(category: Category) -> Completable

    func updateAddress(address: String?) -> Completable

    func getResultPin() -> Single<Pin>

    func clear() -> Completable

}


class EditPinRepoImpl: EditPinRepo {
    private var pin: Pin? = nil

    func setPin(pin: Pin) -> Completable {
        return Completable.create { sub in
            self.pin = pin
            sub(.completed)
            return Disposables.create()
        }
    }

    func createPinWithLocation(location: CLLocation) -> Completable {
        return Completable.create { sub in
            let timestamp = Int64(CACurrentMediaTime() * 1000)
            self.pin = Pin(
                    id: UUID.init().uuidString,
                    meta: Pin.Meta(modified: timestamp, ownerId: "", isSynced: false),
                    content: Pin.Content(title: "", address: "", categoryId: DefaultCategory.name, desc: "", lat: location.coordinate.latitude, lng: location.coordinate.longitude, pictures: [])
            )
            sub(.completed)
            return Disposables.create()
        }
    }

    func updateTitle(title: String) -> Completable {
        return Completable.create { sub in
            self.pin?.content.title = title
            sub(.completed)
            return Disposables.create()
        }
    }

    func updateDescription(description: String) -> Completable {
        return Completable.create { sub in
            self.pin?.content.desc = description
            sub(.completed)
            return Disposables.create()
        }
    }

    func updateCategory(category: Category) -> Completable {
        return Completable.create { sub in
            self.pin?.content.categoryId = category.name
            sub(.completed)
            return Disposables.create()
        }
    }

    func updateAddress(address: String?) -> Completable {
        return Completable
                .create { sub in
            self.pin?.content.address = address
            sub(.completed)
            return Disposables.create()
        }
    }

    func getResultPin() -> Single<Pin> {
        return Single.create { sub in
            let pin = self.pin!
            sub(.success(pin))
            return Disposables.create()
        }
    }

    func clear() -> Completable {
        return Completable.create { sub in
            self.pin = nil
            sub(.completed)
            return Disposables.create()
        }
    }
}
