//
//  ViewPinController.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 09/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift
import iOSMvi
import AnimTypeLabel

class ViewPinController: MviViewController {
    static let PIN_ID_KEY = "pin_id_key"

    override class func getNibName() -> String {
        return "PinDetailsView"
    }

    private var presenter: ViewPinPresenter!
    private let actionsBus = PublishSubject<ViewPinAction>()
    private var compositeDisposable: CompositeDisposable!

    @IBOutlet weak var titleLabel: LocalizableLabel!
    @IBOutlet var categoryLabel: LocalizableLabel!
    @IBOutlet weak var descLabel: LocalizableLabel!
    @IBOutlet weak var addressLabel: LocalizableLabel!

    @IBOutlet weak var addressContent: AnimTypeLabel!
    @IBOutlet weak var titleContent: AnimTypeLabel!
    @IBOutlet weak var descContent: AnimTypeLabel!
    @IBOutlet var categoryIcon: UIImageView!
    @IBOutlet var categoryName: AnimTypeLabel!
    @IBOutlet weak var toolbar: UIToolbar!

    override func viewWillAppear(_ animated: Bool) {

        compositeDisposable = CompositeDisposable()
        configureNavigationBar()

        initPresenter()
        configureViewFields()
        configureToolbar()

        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadData()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        compositeDisposable.dispose()
    }

    private func configureViewFields() {
        titleContent.text = ""
        descContent.text = ""
        categoryName.text = ""
        addressContent.text = ""

        titleLabel.labelStyle()
        descLabel.labelStyle()
        categoryLabel.labelStyle()
        addressLabel.labelStyle()

        addressContent.contentStyle()
        descContent.contentStyle()
        titleContent.contentStyle()
        categoryName.contentStyle()
    }

    private func configureNavigationBar() {
        self.navigationItem.title = LocalizedString("view_pin")
        let editButton = UIBarButtonItem(
                barButtonSystemItem: UIBarButtonSystemItem.edit,
                target: self,
                action: #selector(edit)
        )
        self.navigationItem.rightBarButtonItem = editButton
    }

    private func initPresenter() {
        let container = AppDelegate.container!
        let navigation = Navigator(viewController: self)
        container.registerNavigator(navigator: navigation, key: ViewPinController.getNibName())

        presenter = container.resolve()
        let dis = presenter.subscribe(intentions: actionsBus).observeOn(MainScheduler.instance)
                .subscribe(onNext: { [unowned self] in self.renderView(model: $0) })

        let _ = compositeDisposable.insert(dis)
    }

    func loadData() {
        let pinId = args[ViewPinController.PIN_ID_KEY] as! String
        let action = ViewPin.Load(pinId: pinId)
        actionsBus.onNext(action)
    }

    func configureToolbar() {

        var items: [UIBarButtonItem] = []
        items.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil))
        items.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.action, target: self, action: #selector(share)))
        items.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil))
        items.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.play, target: self, action: #selector(navigate)))
        items.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil))

        toolbar.setItems(items, animated: false)
    }

    func renderView(model: ViewPin.Model) {
        if let pin = model.pin {
            self.titleContent.setAnimatedText(text: pin.content.title)
            self.descContent.setAnimatedText(text: pin.content.desc)
            self.addressContent.setAnimatedText(text: pin.content.address)
            let category = getCategoryByName(name: pin.content.categoryId)
            self.categoryName.setAnimatedText(text: category.name)
            self.categoryIcon.image = UIImage(named: category.icon)

            if pin.content.desc?.isEmpty ?? true { self.descContent.setAnimatedText(text: LocalizedString("no_description")) }
            if pin.content.address?.isEmpty ?? true { self.addressContent.setAnimatedText(text: LocalizedString("no_address")) }
        }
    }

    @objc func edit() { actionsBus.onNext(ViewPin.Edit()) }

    @objc func navigate() { actionsBus.onNext(ViewPin.Navigate()) }

    @objc func share() { actionsBus.onNext(ViewPin.Share(screenName: ViewPinController.getNibName())) }
}
