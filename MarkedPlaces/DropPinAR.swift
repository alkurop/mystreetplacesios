//
//  DropPinAR.swift
//  MyStreetPlaces
//
//  Created by Oleksii.Kuropiatnyk on 13/04/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import GoogleMaps
import iOSMvi

protocol DropPinAction {}
protocol DropPinResult {}

struct DropPin{
    
    //MARK: actions

    struct InitialActionWithPinId : DropPinAction {
        let pinId : String
    }
    struct InitialActionWithLocation : DropPinAction {
        let location: CLLocation
    }
    
    struct BackClick : DropPinAction {}
    struct SubmitClick : DropPinAction {}
    struct DeleteClick : DropPinAction {}
    
    struct TitleUpdated : DropPinAction { let title: String }
    struct AddressUpdated : DropPinAction { let address: String }
    struct DescriptionUpdated : DropPinAction { let description: String }
    struct CategoryUpdated : DropPinAction { let category: Category }
    struct ShowCategoryPicker: DropPinAction {}
    
    // Mark: result

    struct PinLoaded : DropPinResult { let pin: Pin }
    struct ModeResult : DropPinResult { let mode: DropPinMode }
    struct NoTitleResult : DropPinResult {}
    struct ShowCategoryPickerResult: DropPinResult { let pin: Pin }

    //MARK: View model

    struct ViewModel {
        var pin: Pin?
        var deleteButtonVisible: Bool
        var noTitleAlert: OneShot?
        var showCategoryPicker: OneShot?
        var shouldSetDefaultValues: OneShot
    }
}
