//
// Created by Alexey Kuropiantnyk on 24/05/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift

class GoogleNavigatePinUseCase {
    class func execute(pin: Pin) -> Completable {
        let check = CheckHasGoogleMapsUseCase.checkHasGoogleMaps()
                .asObservable().share()
        let deepLink = check.filter { $0 }.flatMap { _ in
            GoogleMapDeepLinkUseCase.execute(pin: pin)
        }.ignoreElements()

        let webLink = check.filter { !$0 }.flatMap { _ in
            GoogleMapWebUseCase.execute(pin: pin)
        }.ignoreElements()

        return Completable.merge([deepLink, webLink])
    }
}