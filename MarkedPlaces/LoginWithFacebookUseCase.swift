//
// Created by Alexey Kuropiantnyk on 29/05/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import FacebookLogin
import RxSwift

class LoginWithFacebookUseCase {

    func execute(viewControllerName: String) -> Single<FacebookLoginResult> {
        let resultPublisher = PublishSubject<FacebookLoginResult>()
        let navigator = navigators[viewControllerName]
        let vc = navigator!.parent!
        let fbManager = LoginManager()

        fbManager.logIn(readPermissions: [.publicProfile], viewController: vc, completion: { (loginResult: LoginResult ) in
            switch loginResult {
                case .failed(let error):
                    resultPublisher.onError(error)
                case .cancelled:
                    resultPublisher.onCompleted()
                case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                    let result = FacebookLoginResult(token: accessToken.authenticationToken)
                    resultPublisher.onNext(result)
            }
        })
        return resultPublisher.asSingle()
                .subscribeOn(MainScheduler.instance)

    }
}

struct FacebookLoginResult {
    let token: String
}

