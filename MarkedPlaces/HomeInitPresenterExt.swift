//
//  HomeInitPresenter.swift
//  My Places
//
//  Created by Alex on 25/07/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import iOSMvi

extension HomeViewController {
    func initPresenter() {
        let container = AppDelegate.container!
        
        let navigation = Navigator(viewController: self)
        container.registerNavigator(navigator: navigation,
                                    key: HomeViewController.getNibName())
        
        presenter = container.resolve()
        
        let mapUpdates = mapUpdateBus
            .flatMap { update -> Observable<HomeAction> in
                
                let observableList: [Observable<HomeAction>] = [
                    Observable.just(Home.LoadPins(bounds: update.bounds)),
                    Observable.just(Home.MapCenterUpdate(center: update.center))
                ]
                
                return Observable.merge(observableList)
        }
        
        let dis = presenter
            .subscribe(intentions: Observable
                .merge([actionsBus
                    .startWith(Home.Initial()),
                        mapUpdates]))
            .observeOn(MainScheduler.instance)
            .subscribe { [unowned self] event in
                if let model = event.element { self.renderView(model) }
        }
        
        let _ = compositeDisposable.insert(dis)
    }
}
