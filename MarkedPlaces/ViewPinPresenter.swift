//
//  ViewPinPresenter.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 09/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import iOSMvi

class ViewPinPresenter: Presenter {

    typealias Action = ViewPinAction
    typealias Result = ViewPinResult
    typealias ViewState = ViewPin.Model
    typealias Interactor1 = ViewPinInteractor

    var interactor: ViewPinInteractor

    init(interactor: ViewPinInteractor) {
        self.interactor = interactor
    }

    func resultToViewStateMapper() -> (ViewPin.Model, ViewPinResult) -> (ViewPin.Model) {
        return { oldModel, result in
            let pin = (result as! ViewPin.LoadedResult).pin
            return ViewPin.Model(pin: pin)
        }
    }

    func defaultViewState() -> ViewPin.Model {
        return ViewPin.Model()
    }
}
