//
//  GoogleMapDeepLinkUseCase.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 24/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift

class GoogleMapDeepLinkUseCase {
    class func execute(pin: Pin) -> Completable {
        return Completable.create { sub in
                    var scheme = "comgooglemaps://?"

                    if let address = pin.content.address {
                        let formatAddress = address.replacingOccurrences(of: " ", with: "+")
                        let addressParam = "q=\(formatAddress)"
                        scheme.append(addressParam)
                    }
                    
                    let centerParamParam = "&center=\(String(format: "%.8f", pin.content.lat)),\(String(format: "%.8f", pin.content.lng))"
                    scheme.append(centerParamParam)

                    let zoomParam = "&zoom=17"
                    scheme.append(zoomParam)

                    let navigationModeParam = "&directionsmode=walking"
                    scheme.append(navigationModeParam)

                    let directionsURL = URL(string: scheme)
                    UIApplication.shared.openURL(directionsURL!)

                    sub(.completed)
                    return Disposables.create()
                }
                .subscribeOn(MainScheduler.instance)
    }
}
