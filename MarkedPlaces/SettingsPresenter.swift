//
// Created by Alexey Kuropiantnyk on 03/06/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import iOSMvi
import RxSwift

class SettingsPresenter: Presenter {

    typealias Action = SettingsAction
    typealias Result = SettingsResult
    typealias ViewState = Settings.ViewModel
    typealias Interactor1 = SettingsInteractor

    var interactor: Interactor1

    init(interactor: Interactor1) {
        self.interactor = interactor
    }

    func resultToViewStateMapper() -> (Settings.ViewModel, SettingsResult) -> (Settings.ViewModel) {
        return { oldState, result in
            return oldState
        }
    }

    func defaultViewState() -> Settings.ViewModel {
        return Settings.ViewModel()
    }
}
