//
// Created by Alexey Kuropiantnyk on 24/05/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift

class GoogleMapWebUseCase {
    class func execute(pin: Pin) -> Completable {
        return Completable.create { sub in
                    let address = createMapUrl(pin: pin)
                    let url = URL(string: address)!
                    UIApplication.shared.open(url)
                    return Disposables.create()
                }
                .subscribeOn(MainScheduler.instance)
    }

    class func createMapUrl(pin: Pin) -> String {
        return "https://maps.google.com/maps?q=loc:\(pin.content.lat),\(pin.content.lng)"
    }
}
