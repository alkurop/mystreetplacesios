//
//  PinsRepo.swift
//  MyStreetPlaces
//
//  Created by Oleksii.Kuropiatnyk on 03/04/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import CoreData
import RxSwift
import GoogleMaps
import iOSLocationUtils

protocol PinsRepo {
    func addOrUpdatePin(pin: Pin) -> Completable
    func removePin(pinId: String) -> Completable
    func searchPins(query: String) -> Single<[Pin]>
    func getPins(projection: [CLLocation]) -> Single<[Pin]>
    func getPinDetails(id: String) -> Single<Optional<Pin>>
}

class PinsRepoImpl: PinsRepo {
    private let context: NSManagedObjectContext

    init(context: NSManagedObjectContext) {
        self.context = context
    }

    func searchPins(query: String) -> Single<[Pin]> {
        return Single.create { single in
            let request: NSFetchRequest<PinMO> = PinMO.fetchRequest()

            if !(query.isEmpty) {
                request.predicate = NSPredicate(format: "title LIKE[cd] %@", query.appending("*"))
            } else {
                request.fetchLimit = 100
            }

            let sortDescriptor = NSSortDescriptor(key: "modified", ascending: false)
            request.sortDescriptors = [sortDescriptor]
            do {
                let result = try self.context.fetch(request).map { $0.toPin() }
                single(.success(result))
            } catch {
                single(.error(error))
            }
            return Disposables.create()
        }
    }

    func observePins(bottomRight: CLLocation, topLeft: CLLocation) -> Single<[Pin]> {
        return Single.create { single in
            let request: NSFetchRequest<PinMO> = PinMO.fetchRequest()
            do {
                let result = try self.context.fetch(request).map { $0.toPin() }
                single(.success(result))
            } catch {
                single(.error(error))
            }
            return Disposables.create()
        }
    }

    func getPinDetails(id: String) -> Single<Optional<Pin>> {
        return Single.create { single in
            let request: NSFetchRequest<PinMO> = PinMO.fetchRequest()
            request.predicate = NSPredicate(format: "id == %@", id)
            do {
                let result = try self.context.fetch(request).map { $0.toPin() }.first
                single(.success(result))
            } catch {
                single(.error(error))
            }
            return Disposables.create()
        }
    }

    func getPins(projection: [CLLocation]) -> Single<[Pin]> {
        return Single.create { single in
            let mapProjection = projection.map { it in it.toULocation() }
            let projectionResult = LocationUtilSwift.convertProjection(
                topRight: mapProjection[0],
                topLeft: mapProjection[1],
                bottomLeft: mapProjection[2],
                bottomRight: mapProjection[3]
            )

            let convertResult = LocationUtilSwift.convertToBounds(ne: projectionResult[0], sw: projectionResult[1])

            let latPredicate = convertResult.latBounds
                .map { bound in
                    [NSPredicate(format: "lat > %f", bound.from),
                        NSPredicate(format: "lat < %f", bound.to)]
                }
                .flatMap { $0 }

            let lngPredicate = convertResult.lngBounds
                .map { bound in
                    [NSPredicate(format: "lng > %f", bound.from),
                        NSPredicate(format: "lng < %f", bound.to)]
                }
                .flatMap { $0 }

            let joinedPredicate = [latPredicate, lngPredicate].flatMap { $0 }

            let request: NSFetchRequest<PinMO> = PinMO.fetchRequest()

            let predicateCompound = NSCompoundPredicate.init(
                type: .and,
                subpredicates: joinedPredicate
            )

            request.predicate = predicateCompound
            do {
                let result = try self.context.fetch(request).map { $0.toPin() }
                single(.success(result))
            } catch {
                single(.error(error))
            }
            return Disposables.create()
        }
    }

    func addOrUpdatePin(pin: Pin) -> Completable {
        return Completable.create { sub in
            let _ = pin.toMO(context: self.context)
            do {
                try self.context.save()
                sub(.completed)
            } catch {
                sub(.error(error))
            }
            return Disposables.create()
        }
    }

    func removePin(pinId: String) -> Completable {
        return Completable.create(subscribe: { completable in
            let request: NSFetchRequest<PinMO> = PinMO.fetchRequest()
            request.predicate = NSPredicate(format: "id == %@", pinId)
            do {
                let result = try self.context.fetch(request).first!
                self.context.delete(result)
                try self.context.save()
                completable(.completed)
            } catch {
                completable(.error(error))
            }
            return Disposables.create()
        })
    }
}
