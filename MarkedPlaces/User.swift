//
// Created by Alexey Kuropiantnyk on 29/05/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import CoreData
import Then

struct User {
    let id: String
    let meta: Meta
    let content: Content

    struct Meta {
        let created: Int64
    }

    struct Content {
        let token: String
        let firstName: String
        let lastName: String
    }
}

extension UserMO {
    func toUser() -> User {
        return User(
                id: self.userId!,
                meta: User.Meta(created: self.created),
                content: User.Content(
                        token: self.token!,
                        firstName: self.firstName!,
                        lastName: self.lastName!
                )
        )
    }
}

extension User: Then {
    func toMo(context: NSManagedObjectContext) -> UserMO {
        let userMo = UserMO(context: context)
                .then { it in
                    it.userId = self.id
                    it.lastName = self.content.lastName
                    it.firstName = self.content.firstName
                    it.token = self.content.token
                }
        return userMo
    }
}