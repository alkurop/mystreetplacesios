//
//  HomeRenderExt.swift
//  My Places
//
//  Created by Alex on 25/07/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import GoogleMaps
import iOSMvi
import RxSwift

extension HomeViewController {
    func renderView(_ viewModel: Home.ViewModel) {
        if (viewModel.showMapCenterUnknownDialog?.isActive() ?? false) {
            showCannotDropPinDialog()
        }
        if let pins = viewModel.pins {
            let includedMarkers = getIncludedClusterItems(markers: markers, pins: pins)
            let excludedMarkers = getExcludedClusterItems(markers: markers, pins: pins)
            
            excludedMarkers.forEach { (id, marker) in
                markers.removeValue(forKey: id);
                clusterManager!.remove(marker)
            }
            includedMarkers.forEach { (id, marker) in
                markers[id] = marker;
                clusterManager!.add(marker)
            }
            
            if !(includedMarkers.isEmpty && excludedMarkers.isEmpty) {
                clusterManager!.cluster()
            }
            clusterManager?.setDelegate(self, mapDelegate: self)
        }
        
        if let focusTo = viewModel.focusTo {
            focusToLocation(lat: focusTo.coordinate.latitude, lng: focusTo.coordinate.longitude)
        }
    }
}
