//
//  AppDelegate.swift
//  MyStreetPlaces
//
//  Created by Катяя Куропятник on 02.04.18.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import DITranquillity
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static var container: DIContainer!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        Fabric.with([Crashlytics.self])

        GMSServices.provideAPIKey("AIzaSyAfstYZpbU3OFLlpRT5Saw7HDYK3_2ayuo")
        let bounds = UIScreen.main.bounds
        self.window = UIWindow(frame: bounds)
        self.window?.backgroundColor = UIColor(ciColor: CIColor.white)

        let _ = StorageImpl {
            AppDelegate.self.container = buildComponent(context: $0)
            let homeViewController = HomeViewController(nibName: HomeViewController.getNibName(), bundle: nil)
            let navigationController = UINavigationController(rootViewController: homeViewController)
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
        }
        return true
    }
}

