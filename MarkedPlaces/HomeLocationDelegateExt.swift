//
//  HomeLocationDelegate.swift
//  My Places
//
//  Created by Alex on 25/07/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import GoogleMaps
import iOSMvi
import RxSwift

extension HomeViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status != .denied && status != .restricted else {
            let alert = UIAlertController(
                title: LocalizedString("no_location_title"),
                message: LocalizedString("no_location_msg"),
                preferredStyle: UIAlertControllerStyle.alert
            )
            
            alert.addAction(
                UIAlertAction(
                    title: LocalizedString("ok"),
                    style: UIAlertActionStyle.default,
                    handler: { action in self.showSettings() })
            )
            alert.addAction(
                UIAlertAction(
                    title: LocalizedString("cancel"),
                    style: UIAlertActionStyle.cancel,
                    handler: { action in alert.dismiss(animated: true, completion: nil) })
            )
            self.present(alert, animated: true)
            return
        }
        
        locationManager.startUpdatingLocation()
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
    }
    
    func showSettings() {
        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if focused {
            return
        }
        guard let location = locations.first else {
            return
        }
        mapView.camera = GMSCameraPosition(
            target: location.coordinate,
            zoom: 17,
            bearing: 0,
            viewingAngle: 0)
        focused = true
    }
}
