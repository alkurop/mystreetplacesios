//
// Created by Alexey Kuropiantnyk on 23/05/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation

class ClusterRenderer: GMUDefaultClusterRenderer {
    override func shouldRender(as cluster: GMUCluster, atZoom zoom: Float) -> Bool {
        if zoom >= 20 {
            return false
        } else{
            return cluster.count >= 2
        }
    }
}

class ClusterRendererDelegate: NSObject, GMUClusterRendererDelegate {
    @objc func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        if (marker.userData is IdClusterItem) {
            let markerData = marker.userData as! IdClusterItem
            marker.iconView = markerData.getIconView()
            marker.title = markerData.title
        }
    }
}
