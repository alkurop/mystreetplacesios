//
//  LatestProjectionRepo.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 08/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import GoogleMaps

protocol LatestProjectionRepo {
    func observe() -> Observable<Optional<[CLLocation]>>

    func update(projection: [CLLocation]) -> Completable
}


class LatestProjectionRepoImpl: LatestProjectionRepo {
    let centerSubject = BehaviorSubject<Optional<[CLLocation]>>(value: Optional.none)

    func observe() -> Observable<Optional<[CLLocation]>> {
        return centerSubject
    }

    func update(projection: [CLLocation]) -> Completable {
        return Completable.create { sub in
            self.centerSubject.onNext(Optional.some(projection))
            sub(.completed)
            return Disposables.create()
        }
    }
}
