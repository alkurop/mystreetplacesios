//
//  HomeInitMapsExt.swift
//  My Places
//
//  Created by Alex on 25/07/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import GoogleMaps
import iOSMvi
import RxSwift

extension HomeViewController {
    func initMaps() {
        do {
            let styleURL = Bundle.main.url(forResource: "GoogleMapStyle",
                                           withExtension: "json")
            try mapView.mapStyle = GMSMapStyle(contentsOfFileURL: styleURL!)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        mapView.settings.indoorPicker = false
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        if (clusterManager == nil) {
            let iconGenerator = GMUDefaultClusterIconGenerator()
            let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
            let renderer = ClusterRenderer(mapView: mapView,
                                           clusterIconGenerator: iconGenerator)
            rendererDelegate = ClusterRendererDelegate()
            renderer.delegate = rendererDelegate
            clusterManager = GMUClusterManager(map: mapView, algorithm:
                algorithm, renderer: renderer)
        }
    }
}
