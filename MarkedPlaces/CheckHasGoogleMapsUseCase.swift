//
//  CheckGoogleMapsInstalled.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 24/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift
import UIKit

class CheckHasGoogleMapsUseCase {
    class func checkHasGoogleMaps() -> Single<Bool> {
        return Single.create { sub in
            let googleMapsUrlScheme = "comgooglemaps-x-callback://"
            let canOpenGoogleMaps = CheckHasGoogleMapsUseCase.schemeAvailable(scheme: googleMapsUrlScheme)
            sub(.success(canOpenGoogleMaps))
            return Disposables.create()
        }
    }

    private class func schemeAvailable(scheme: String) -> Bool {
        if let url = URL(string: scheme) {
            return UIApplication.shared.canOpenURL(url)
        }
        return false
    }

}
