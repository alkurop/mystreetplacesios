//
//  SearchTableCell.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 12/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import UIKit

class SearchTableCell: UITableViewCell {

    class func getNibName() -> String { return "SearchCellView" }

    class func getIdentifier() -> String { return "searchCell" }

    var clickListener: ((Int) -> ())? = nil

    @IBOutlet weak var container: UIView!
    @IBOutlet weak var categotyView: UIImageView!
    @IBOutlet weak var labelView: UILabel!
    
    @objc func onClick() {
        let index = (self.superview as! UITableView).indexPath(for: self)
        let intIndex = index!.row
        clickListener?(intIndex)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        container.layer.cornerRadius = 10
        let noGesture = UITapGestureRecognizer(target: self, action: #selector(onClick as () -> Void))
        container.addGestureRecognizer(noGesture)
    }
}
