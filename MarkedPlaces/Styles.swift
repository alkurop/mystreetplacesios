//
//  Styles.swift
//  MyStreetPlaces
//
//  Created by Alexey Kuropiantnyk on 12/05/2018.
//  Copyright © 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import UIKit

extension UITextView {
    func heightToFit() {
        let maxHeight = 200.0
        let input = self
        input.translatesAutoresizingMaskIntoConstraints = true
        let oldFrame = input.frame
        input.sizeToFit()
        var updateFrame = input.frame
        updateFrame.size.width = oldFrame.width
        if updateFrame.size.height > CGFloat(maxHeight) {
            updateFrame.size.height = CGFloat(maxHeight)
        }
        input.frame = updateFrame
    }

    func roundBorderStyle() {
        self.layer.cornerRadius = 5.0
        self.clipsToBounds = true
        self.layer.borderColor = UIColor.gray.withAlphaComponent(0.2).cgColor
        self.layer.borderWidth = 0.5
    }
}

extension UILabel {
    func heightToFit() {
        let maxHeight = 200.0
        let input = self
        input.translatesAutoresizingMaskIntoConstraints = true
        let oldFrame = input.frame
        input.sizeToFit()
        var updateFrame = input.frame
        updateFrame.size.width = oldFrame.width
        if updateFrame.size.height > CGFloat(maxHeight) {
            updateFrame.size.height = CGFloat(maxHeight)
        }
        input.frame = updateFrame
    }

    func labelStyle() {
        self.font = self.font.withSize(12)
        let heightConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.height, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 17)
        self.addConstraint(heightConstraint)
    }

    func contentStyle() {
        self.font =  self.font.withSize(16)
        let heightConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.height, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 23)
        self.addConstraint(heightConstraint)
    }
}
