//
// Created by Alexey Kuropiantnyk on 29/05/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation
import RxSwift

class LoginOnBackendUseCase {

    func execute(token: String) -> Completable {
        return Completable.create { sub in
            print(token)
            return Disposables.create()
        }
    }
}
