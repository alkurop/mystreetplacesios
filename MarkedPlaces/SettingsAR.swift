//
// Created by Alexey Kuropiantnyk on 03/06/2018.
// Copyright (c) 2018 Катяя Куропятник. All rights reserved.
//

import Foundation

protocol SettingsAction {}
protocol SettingsResult {}

struct Settings
{
    //View model
    struct ViewModel {}
}